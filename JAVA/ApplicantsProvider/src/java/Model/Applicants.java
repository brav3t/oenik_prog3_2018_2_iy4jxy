package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.xml.bind.annotation.*;

/**
 *
 * @author brave
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Applicants implements java.io.Serializable{
    
    static final long serialVersionUID = 1L;
    
    static Random rnd = new Random();
    
    String[] lastNames = new String[]{"Nagy","Kovács","Tóth","Szabó","Horváth","Varga","Kiss","Molnár" };
    
    String[] firstNames = new String[]{"János","Péter","György","Mihály","Katalin","Ilona","Ágota","Margit"};
    
    String[] vehicleBrands = new String[]{"Volkswagen","Ford","Renault","Tesla","Opel","Peugeot","Skoda","Fiat"};
    
    String[] vehicleModels = new String[]{"Spark","Mars","Zoom","Falcon","R2","Zeus","Smoker","Chrono"};
    
    String[] vehicleColors = new String[]{"piros","narancs","sárga","kék","zöld","fekete","fehér"};

    @XmlElement
    List<Applicant> applicants = new ArrayList<>();
        
    public Applicants() {
        this.PopulateApplicants();
    }    
    
    void PopulateApplicants(){        
        int applicantsNumber = rnd.nextInt(21)+1;                
        
        String firstName;
        String lastName;
        int experience;
        
        for (int i = 0; i < applicantsNumber; i++) {
            firstName = this.firstNames[rnd.nextInt(this.firstNames.length)];
            lastName = this.lastNames[rnd.nextInt(this.lastNames.length)];
            experience = rnd.nextInt(8) + 3;            
            
            this.applicants.add(new Applicant(firstName, lastName, experience, this.GenerateVehicle()));
        }        
    }
     
    Vehicle GenerateVehicle(){
        String brand = this.vehicleBrands[rnd.nextInt(this.vehicleBrands.length)];
        String model = this.vehicleModels[rnd.nextInt(this.vehicleModels.length)];
        String color = this.vehicleColors[rnd.nextInt(this.vehicleColors.length)];
        int ageGroup = rnd.nextInt(120) + 1981;
        int speedometerState = rnd.nextInt(500001);
        
        return new Vehicle(brand, model, color, ageGroup, speedometerState);        
    }    
}
