package Model;

import javax.xml.bind.annotation.*;

/**
 *
 * @author brave
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Vehicle implements java.io.Serializable {
    
    static final long serialVersionUID = 1L;
    
    @XmlElement
    String brand;
    
    @XmlElement
    String model;
    
    @XmlElement
    String color;
    
    @XmlElement
    int ageGroup;
    
    @XmlElement
    int speedometerState;
    
    public Vehicle(String brand, String model, String color, int ageGroup, int speedometerState) {
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.ageGroup = ageGroup;
        this.speedometerState = speedometerState;
    }    
}
