package Model;

import javax.xml.bind.annotation.*;

/**
 *
 * @author brave
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Applicant implements java.io.Serializable {
    
    static final long serialVersionUID = 1L;
    
    @XmlElement
    String firstName;
    
    @XmlElement
    String lastName;
    
    @XmlElement
    int experience;
    
    @XmlElement
    Vehicle vehicle;

    public Applicant(String firstName, String lastName, int experience, Vehicle vehicle) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
        this.vehicle = vehicle;
    }    
}
