var class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests =
[
    [ "AddNewItem_Adds_Correct_Address_To_AddressRepo", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#a236fd8aacfe3e827985549fba13b4a48", null ],
    [ "AddNewItem_Adds_Only_One_Address_To_Repository", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#acef9e1c5932144d59cc5cd3bba99d429", null ],
    [ "AddNewItem_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#ab1c945d6423311fc26aeb2e5c7c59428", null ],
    [ "AddNewItem_Successfull_Insert_Returns_Correct_String_", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#a39687e20806147aa6add3146edf4183a", null ],
    [ "ListItems_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#af4bf80703d5105c1c951cedc65b0b19a", null ],
    [ "ListItems_Returns_String_Contains_Db_Data", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#a24dd503e4edaf77ab4c52b108ca4cf8e", null ],
    [ "RemoveItem_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#ad33b117cc671bda2a1c9eadb7104fe55", null ],
    [ "RemoveItem_If_ID_Not_In_Db_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#a261ab5ae5d2163e20fcc8552a55dca8c", null ],
    [ "RemoveItem_Removes_Correct_Address", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#a81b52e1c00de67b54ba1dbd31f9639ee", null ],
    [ "RemoveItem_Removes_Only_One_Address", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#a0e2de47b3926d9aab874def07d29a747", null ],
    [ "RemoveItem_Successfull_Remove_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#a09b44dfc5c22c6df6c7e6cdf47df033e", null ],
    [ "SetupAddressRepo", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#a127fe905c0b6ad57a63bb206c4f7c615", null ],
    [ "UpdateItem_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#aee0492a4c585a9a0d39fea628055f51e", null ],
    [ "UpdateItem_If_ID_Not_In_Db_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#ab343a55cc4ff9d05958f3cdd5719daa4", null ],
    [ "UpdateItem_Succesfull_Update_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#a6830d84ad504774f11d1af6d6ee9e1b3", null ],
    [ "UpdateItem_Updates_CITY_Column", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#ab4f75cb3e4a8619b0e2f439c3c6dc4a8", null ],
    [ "UpdateItem_Updates_STREET_NAME_Column", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#a02954376637232be0f741cbf81b856e9", null ],
    [ "UpdateItem_Updates_STREET_NUMBER_Column", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#a0e687ead35e013e476a6e9098971b9fd", null ],
    [ "UpdateItem_Updates_STREET_TYPE_Column", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#a5444a5526677dec4655ad777bcb29535", null ],
    [ "UpdateItem_Updates_ZIP_Column", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html#acca5056269692d95c5b29df0d144c185", null ]
];