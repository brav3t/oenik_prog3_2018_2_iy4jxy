var class_z_t_1_1_logic_1_1_logics_1_1_crud_logic =
[
    [ "CrudLogic", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html#ad1a555b7b16846c4f0790543fb4986f2", null ],
    [ "AddNewItem", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html#a1242ea886eae7d73a78c7114bbb0f3be", null ],
    [ "ListItems", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html#acca1ed08204947fe84d0c38b40ca9e7f", null ],
    [ "RemoveItem", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html#a2f5424da5c122cf6abe26ea4d3819cdd", null ],
    [ "UpdateItem", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html#ac32b52b6dbf74ead78cad708c00018f3", null ],
    [ "Repo", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html#a67975a27059baddba50d67ee4b293e39", null ]
];