var class_z_t_1_1_repository_1_1_repository =
[
    [ "Repository", "class_z_t_1_1_repository_1_1_repository.html#a5a193e42d691f14c4b04fe27fec658b7", null ],
    [ "GetAll", "class_z_t_1_1_repository_1_1_repository.html#ac6e6935b985c40721d2bdd0aa5e2aa43", null ],
    [ "GetItemById", "class_z_t_1_1_repository_1_1_repository.html#af952a7e966ce912d7901a7f38d68c688", null ],
    [ "Insert", "class_z_t_1_1_repository_1_1_repository.html#afb3b9a097ce64660419f7510e7b35aed", null ],
    [ "Remove", "class_z_t_1_1_repository_1_1_repository.html#aeff926a74d85fdcb20da9340aac8cd38", null ],
    [ "Update", "class_z_t_1_1_repository_1_1_repository.html#af7950c3c965d9f2e988db77c901341d5", null ],
    [ "DbSet", "class_z_t_1_1_repository_1_1_repository.html#ac54ec059b555c096f1056726e4ed17f7", null ],
    [ "ZT", "class_z_t_1_1_repository_1_1_repository.html#a25b99ea098aec0191adb477752eb8a51", null ]
];