var class_zuber_transport_1_1_menu_pages_1_1_template_page =
[
    [ "TemplatePage", "class_zuber_transport_1_1_menu_pages_1_1_template_page.html#a4914ace995b4f283e6ee558f833b78c6", null ],
    [ "AddSelected", "class_zuber_transport_1_1_menu_pages_1_1_template_page.html#a6b621cd99d4434ea986aa4138aa14b09", null ],
    [ "Display", "class_zuber_transport_1_1_menu_pages_1_1_template_page.html#a168f6b609123af005028c0e8790d69bc", null ],
    [ "ModifySelected", "class_zuber_transport_1_1_menu_pages_1_1_template_page.html#a91ea7f03dbdb52aa68348f83ce08a356", null ],
    [ "RefreshPage", "class_zuber_transport_1_1_menu_pages_1_1_template_page.html#a3b22dff98f6a5e980930a5a8dc7565f2", null ],
    [ "RemoveSelected", "class_zuber_transport_1_1_menu_pages_1_1_template_page.html#acb92e0030f68be65cc26e14884e503c3", null ],
    [ "BL", "class_zuber_transport_1_1_menu_pages_1_1_template_page.html#a6a5505a75004adc004a63e5cdb6994eb", null ],
    [ "PageName", "class_zuber_transport_1_1_menu_pages_1_1_template_page.html#a95e2ab9248b1ab6060e7afae6837f530", null ]
];