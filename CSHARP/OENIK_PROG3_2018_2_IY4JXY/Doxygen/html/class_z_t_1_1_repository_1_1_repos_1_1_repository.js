var class_z_t_1_1_repository_1_1_repos_1_1_repository =
[
    [ "Repository", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html#aba76de3f593419773917981a6dd9df7a", null ],
    [ "GetAll", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html#a185ea2b250f9a8cf2a789e725e11cc0f", null ],
    [ "GetItemById", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html#ac63926f0a74068f7303148b352f56b9d", null ],
    [ "Insert", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html#a30270febaebcc0cd4d2510299c1874d0", null ],
    [ "Remove", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html#a06c9141600106fd42a68905bd5d54acf", null ],
    [ "Update", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html#a9d3f502194fecd3ca2be63d6218204d1", null ],
    [ "DbSet", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html#ac7e94d769215c73f07b14ebaf952d16b", null ],
    [ "ZT", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html#ae4169b7e281a2bcfae60f31dd3a1ec32", null ]
];