var class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t =
[
    [ "ADDRESS", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#a4b6e6bc794a0c5006613b61dde21f010", null ],
    [ "ADDRESS1", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#ad47d688141bddbab6995f007fe364745", null ],
    [ "ARRIVE_TIME", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#a0f0c5fe99945f590faf188a8cb8d8baf", null ],
    [ "DEPARTURE_ADDR_ID", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#a8b245bfbe50d7720078bd21fe1c07884", null ],
    [ "DEPARTURE_TIME", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#a3b075e7f08fd416baa5e4c929f583b90", null ],
    [ "DESTINATION_ADDR_ID", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#a3117377cb8f0cb83d066e3082dc9b8ab", null ],
    [ "DRIVER", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#a87a17143fcad9f113a1ab611ec9c5625", null ],
    [ "DRIVER_ID", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#a63b44395627b6687e8e2f12a86e162c4", null ],
    [ "ID_TRANSPORT", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#a4564d80c31d1e0e17ba4d0d072828ee0", null ],
    [ "PASSENGER", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#ae49002ac80a19c644b876e6facfd2293", null ],
    [ "PASSENGER_ID", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#a47f7515c5cf3af158cf97981b6af6cd3", null ],
    [ "VEHICLE", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#af302dcf748bc31e4eb0d195aa940f776", null ],
    [ "VEHICLE_ID", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html#afc24a1421962ff05e28708b9c2366093", null ]
];