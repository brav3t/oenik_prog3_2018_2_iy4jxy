var hierarchy =
[
    [ "ZT.Data.ADDRESS", "class_z_t_1_1_data_1_1_a_d_d_r_e_s_s.html", null ],
    [ "ZT.Logic.Tests.Tests.AddressLogicTests", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_address_logic_tests.html", null ],
    [ "ZT.Logic.Logics.BusinessLogic", "class_z_t_1_1_logic_1_1_logics_1_1_business_logic.html", null ],
    [ "ZT.Logic.Tests.Tests.BusinessLogicTests", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests.html", null ],
    [ "ZT.Logic.Logics.CrudLogic< ADDRESS >", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html", [
      [ "ZT.Logic.Logics.AddressLogic", "class_z_t_1_1_logic_1_1_logics_1_1_address_logic.html", null ]
    ] ],
    [ "ZT.Logic.Logics.CrudLogic< DRIVER >", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html", [
      [ "ZT.Logic.Logics.DriversLogic", "class_z_t_1_1_logic_1_1_logics_1_1_drivers_logic.html", null ]
    ] ],
    [ "ZT.Logic.Logics.CrudLogic< PASSENGER >", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html", [
      [ "ZT.Logic.Logics.PassengersLogic", "class_z_t_1_1_logic_1_1_logics_1_1_passengers_logic.html", null ]
    ] ],
    [ "ZT.Logic.Logics.CrudLogic< TRANSPORT >", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html", [
      [ "ZT.Logic.Logics.TransportsLogic", "class_z_t_1_1_logic_1_1_logics_1_1_transports_logic.html", null ]
    ] ],
    [ "ZT.Logic.Logics.CrudLogic< VEHICLE >", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html", [
      [ "ZT.Logic.Logics.VehiclesLogic", "class_z_t_1_1_logic_1_1_logics_1_1_vehicles_logic.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "ZT.Data.ZTEntities", "class_z_t_1_1_data_1_1_z_t_entities.html", null ]
    ] ],
    [ "ZT.Data.DRIVER", "class_z_t_1_1_data_1_1_d_r_i_v_e_r.html", null ],
    [ "ZT.Logic.Tests.Tests.DriversLogicTests", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_drivers_logic_tests.html", null ],
    [ "ZT.Logic.ILogic", "interface_z_t_1_1_logic_1_1_i_logic.html", [
      [ "ZT.Logic.Logics.CrudLogic< T >", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html", null ]
    ] ],
    [ "ZT.Repository.IRepository< T >", "interface_z_t_1_1_repository_1_1_i_repository.html", [
      [ "ZT.Repository.Repos.Repository< T >", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html", null ]
    ] ],
    [ "ZT.Repository.IRepository< ZT.Data.ADDRESS >", "interface_z_t_1_1_repository_1_1_i_repository.html", null ],
    [ "ZT.Repository.IRepository< ZT.Data.DRIVER >", "interface_z_t_1_1_repository_1_1_i_repository.html", null ],
    [ "ZT.Repository.IRepository< ZT.Data.PASSENGER >", "interface_z_t_1_1_repository_1_1_i_repository.html", null ],
    [ "ZT.Repository.IRepository< ZT.Data.TRANSPORT >", "interface_z_t_1_1_repository_1_1_i_repository.html", null ],
    [ "ZT.Repository.IRepository< ZT.Data.VEHICLE >", "interface_z_t_1_1_repository_1_1_i_repository.html", null ],
    [ "MenuPage", null, [
      [ "ZuberTransport.MenuPages.MainPage", "class_zuber_transport_1_1_menu_pages_1_1_main_page.html", null ]
    ] ],
    [ "Page", null, [
      [ "ZuberTransport.MenuPages.BusinessPage", "class_zuber_transport_1_1_menu_pages_1_1_business_page.html", null ],
      [ "ZuberTransport.MenuPages.ImportDataPage", "class_zuber_transport_1_1_menu_pages_1_1_import_data_page.html", null ],
      [ "ZuberTransport.MenuPages.TemplatePage", "class_zuber_transport_1_1_menu_pages_1_1_template_page.html", [
        [ "ZuberTransport.MenuPages.AddressesPage", "class_zuber_transport_1_1_menu_pages_1_1_addresses_page.html", null ],
        [ "ZuberTransport.MenuPages.DriversPage", "class_zuber_transport_1_1_menu_pages_1_1_drivers_page.html", null ],
        [ "ZuberTransport.MenuPages.PassengersPage", "class_zuber_transport_1_1_menu_pages_1_1_passengers_page.html", null ],
        [ "ZuberTransport.MenuPages.TransportsPage", "class_zuber_transport_1_1_menu_pages_1_1_transports_page.html", null ],
        [ "ZuberTransport.MenuPages.VehiclesPage", "class_zuber_transport_1_1_menu_pages_1_1_vehicles_page.html", null ]
      ] ]
    ] ],
    [ "ZT.Data.PASSENGER", "class_z_t_1_1_data_1_1_p_a_s_s_e_n_g_e_r.html", null ],
    [ "ZT.Logic.Tests.Tests.PassengersLogicTests", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_passengers_logic_tests.html", null ],
    [ "Program", null, [
      [ "ZuberTransport.MainMenu", "class_zuber_transport_1_1_main_menu.html", null ]
    ] ],
    [ "ZT.Repository.Repos.Repository< ADDRESS >", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html", [
      [ "ZT.Repository.Repos.AddressRepo", "class_z_t_1_1_repository_1_1_repos_1_1_address_repo.html", null ]
    ] ],
    [ "ZT.Repository.Repos.Repository< DRIVER >", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html", [
      [ "ZT.Repository.Repos.DriversRepo", "class_z_t_1_1_repository_1_1_repos_1_1_drivers_repo.html", null ]
    ] ],
    [ "ZT.Repository.Repos.Repository< PASSENGER >", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html", [
      [ "ZT.Repository.Repos.PassengersRepo", "class_z_t_1_1_repository_1_1_repos_1_1_passengers_repo.html", null ]
    ] ],
    [ "ZT.Repository.Repos.Repository< TRANSPORT >", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html", [
      [ "ZT.Repository.Repos.TransportsRepo", "class_z_t_1_1_repository_1_1_repos_1_1_transports_repo.html", null ]
    ] ],
    [ "ZT.Repository.Repos.Repository< VEHICLE >", "class_z_t_1_1_repository_1_1_repos_1_1_repository.html", [
      [ "ZT.Repository.Repos.VehiclesRepo", "class_z_t_1_1_repository_1_1_repos_1_1_vehicles_repo.html", null ]
    ] ],
    [ "ZuberTransport.Runner", "class_zuber_transport_1_1_runner.html", null ],
    [ "ZT.Data.TRANSPORT", "class_z_t_1_1_data_1_1_t_r_a_n_s_p_o_r_t.html", null ],
    [ "ZT.Logic.Tests.Tests.TransportsLogicTests", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_transports_logic_tests.html", null ],
    [ "ZT.Data.VEHICLE", "class_z_t_1_1_data_1_1_v_e_h_i_c_l_e.html", null ],
    [ "ZT.Logic.Tests.Tests.VehicleLogicTests", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html", null ]
];