var class_z_t_1_1_data_1_1_v_e_h_i_c_l_e =
[
    [ "VEHICLE", "class_z_t_1_1_data_1_1_v_e_h_i_c_l_e.html#ab14a6981ec9d96ee912de635356e7034", null ],
    [ "AGE_GROUP", "class_z_t_1_1_data_1_1_v_e_h_i_c_l_e.html#a084c06232335c61f9272973b37c56d71", null ],
    [ "BRAND", "class_z_t_1_1_data_1_1_v_e_h_i_c_l_e.html#a2259307db59734d25f81dcd916cdebf9", null ],
    [ "COLOR", "class_z_t_1_1_data_1_1_v_e_h_i_c_l_e.html#adc59550dce810ac794b40e9039af30df", null ],
    [ "ID_VEHICLE", "class_z_t_1_1_data_1_1_v_e_h_i_c_l_e.html#ae8ea289e438dea7c0de4034d1f0683eb", null ],
    [ "MODELL", "class_z_t_1_1_data_1_1_v_e_h_i_c_l_e.html#aea619d80b98f327a0a221912c02dff6b", null ],
    [ "SPEEDOMETER_STATE", "class_z_t_1_1_data_1_1_v_e_h_i_c_l_e.html#a4a4a53d2d76707e8dd859bbd16f5dc33", null ],
    [ "TRANSPORTS", "class_z_t_1_1_data_1_1_v_e_h_i_c_l_e.html#a9905532013101c5e04c4ade438f8fa67", null ]
];