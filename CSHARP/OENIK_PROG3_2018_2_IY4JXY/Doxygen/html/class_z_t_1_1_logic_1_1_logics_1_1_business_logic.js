var class_z_t_1_1_logic_1_1_logics_1_1_business_logic =
[
    [ "BusinessLogic", "class_z_t_1_1_logic_1_1_logics_1_1_business_logic.html#ab2e0d987a87e9e040b152b6c0c10ee15", null ],
    [ "CreateTransportsView", "class_z_t_1_1_logic_1_1_logics_1_1_business_logic.html#a5e15b1fd38b1c589d9764911f2085fa9", null ],
    [ "GetAverageCarAge", "class_z_t_1_1_logic_1_1_logics_1_1_business_logic.html#ad309f7a4dd77609d13ea08df3c3e3fe8", null ],
    [ "GetTop3Customers", "class_z_t_1_1_logic_1_1_logics_1_1_business_logic.html#a09236a61c0447bed14d25d0590924f59", null ],
    [ "GetTop3DriversByDrives", "class_z_t_1_1_logic_1_1_logics_1_1_business_logic.html#ab534a965ce0806594eb933dadba82761", null ],
    [ "GetTop3DriversByPoint", "class_z_t_1_1_logic_1_1_logics_1_1_business_logic.html#af97f6e29016471b4b33a251c57f738c7", null ],
    [ "GiveDriverPoints", "class_z_t_1_1_logic_1_1_logics_1_1_business_logic.html#a0360d6353e4999b91cc07170f2c17d01", null ]
];