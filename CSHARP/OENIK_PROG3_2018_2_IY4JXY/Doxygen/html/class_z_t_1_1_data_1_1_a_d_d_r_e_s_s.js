var class_z_t_1_1_data_1_1_a_d_d_r_e_s_s =
[
    [ "ADDRESS", "class_z_t_1_1_data_1_1_a_d_d_r_e_s_s.html#a6a51fc926fcff5976165ad43a2d5a8ec", null ],
    [ "CITY", "class_z_t_1_1_data_1_1_a_d_d_r_e_s_s.html#a5a5eb1f440ef6372a2e7f14f657b5eaf", null ],
    [ "ID_ADDRESS", "class_z_t_1_1_data_1_1_a_d_d_r_e_s_s.html#a657398e03e193b5271f4a0ae4156626f", null ],
    [ "PASSENGERS", "class_z_t_1_1_data_1_1_a_d_d_r_e_s_s.html#aca581d333a13f2f7c047287765282508", null ],
    [ "STREET_NAME", "class_z_t_1_1_data_1_1_a_d_d_r_e_s_s.html#a70fc125011da7ec163776e72e8dc6d98", null ],
    [ "STREET_NUMBER", "class_z_t_1_1_data_1_1_a_d_d_r_e_s_s.html#a9d5cac634ce230b3fadf7fc3b2506342", null ],
    [ "STREET_TYPE", "class_z_t_1_1_data_1_1_a_d_d_r_e_s_s.html#a4b16ccab8adf2dec7f91b41fb0f46cc2", null ],
    [ "TRANSPORTS", "class_z_t_1_1_data_1_1_a_d_d_r_e_s_s.html#a91eef2d278f083f176e8df9583c40f9e", null ],
    [ "TRANSPORTS1", "class_z_t_1_1_data_1_1_a_d_d_r_e_s_s.html#afb6623511e02c431ecf7137b3fbe28d7", null ],
    [ "ZIP_CODE", "class_z_t_1_1_data_1_1_a_d_d_r_e_s_s.html#a35e1dee91e3e1e78e8841bfc2ebb6601", null ]
];