var class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests =
[
    [ "CreateTransportView_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests.html#af059a81522a04109402802266a8910ac", null ],
    [ "CreateTransportView_Returns_String_Contains_Db_Data", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests.html#a88031886f4f876518496000dd06513fd", null ],
    [ "GetAverageCarAge_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests.html#a4a5db733b002f5ed479646fb725fccef", null ],
    [ "GetAverageCarAge_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests.html#a360ced0589efe1f8d582dbd41b4d5127", null ],
    [ "GetTop3Customers_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests.html#aac8805c68cf73a78c6af726b301d1d1c", null ],
    [ "GetTop3Customers_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests.html#a726b1977d8f6c71f4527e9fb77e425c5", null ],
    [ "GetTop3DriversByDrives_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests.html#a517b14abcf81b9495059a2467b7fbce3", null ],
    [ "GetTop3DriversByDrives_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests.html#a8b1bd66c31c23ced9383c4712c8d9fae", null ],
    [ "GetTop3DriversByPoint_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests.html#a4712c61416692a8550c40a103759fa57", null ],
    [ "GetTop3DriversByPoints_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests.html#a58d5985ae58fc2425bab047097f7b064", null ],
    [ "SetupBusinessLogicRepos", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_business_logic_tests.html#a24087159a49672d473f173279ce0b9b1", null ]
];