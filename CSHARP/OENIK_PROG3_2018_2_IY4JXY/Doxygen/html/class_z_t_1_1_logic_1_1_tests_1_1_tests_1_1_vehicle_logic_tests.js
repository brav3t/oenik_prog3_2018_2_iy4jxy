var class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests =
[
    [ "AddNewItem_Adds_Correct_Item_To_DriversRepo", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a770b316797c417cfd2018b7bbd7b2617", null ],
    [ "AddNewItem_Adds_Only_One_Item_To_Repository", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#aa3bdb0ab19ba4aefe48f43db6984cd1f", null ],
    [ "AddNewItem_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#af5243c447e65e21528e2214c566e28e3", null ],
    [ "AddNewItem_Successfull_Insert_Returns_Correct_String_", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a77bddfe85baf5030df2454297bd72f38", null ],
    [ "ListItems_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a3ca878da06f23af6b1b70e2c3db54878", null ],
    [ "ListItems_Return_String_Contains_Db_Data", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a364767eb996882c087a79569a9fbe2a2", null ],
    [ "RemoveItem_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a8ec217fb188d4047453c52ccafca1a8a", null ],
    [ "RemoveItem_If_ID_Not_In_Db_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a1162fc5ef32b606eddce9134eba261be", null ],
    [ "RemoveItem_Removes_Correct_Item", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#ac55b7d98cbdd6dfaaf4280972bde34c7", null ],
    [ "RemoveItem_Removes_Only_One_Item", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a62bb5671aec44a677ffff752343d5105", null ],
    [ "RemoveItem_Successfull_Remove_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#af67aebbc23ea5badc7a12336eaa06fe5", null ],
    [ "SetupVehiclesRepo", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#aa15c0f6329889afaef96f9584244ad44", null ],
    [ "UpdateItem_If_Exception_Thrown_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a426b995110e1abf329662b971397404a", null ],
    [ "UpdateItem_If_ID_Not_In_Db_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a3525c6c01b46f55a2fef76cae5f55e5a", null ],
    [ "UpdateItem_Succesfull_Update_Returns_Correct_String", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a63308dfbb2af1df234da5aead3a99ce6", null ],
    [ "UpdateItem_Updates_AGE_GROUP_Column", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a25c309c2ad2a652d9c31ee89dff30afa", null ],
    [ "UpdateItem_Updates_BRAND_Column", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a0c2ebea9254fc266bed3419257eacd77", null ],
    [ "UpdateItem_Updates_COLOR_Column", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#acba5b5cc094d6e2427da7144d9c16558", null ],
    [ "UpdateItem_Updates_MODEL_Column", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a0e1f714d3c79fd1b5ddb11328ea03a31", null ],
    [ "UpdateItem_Updates_SPEEDOMETER_STATE_Column", "class_z_t_1_1_logic_1_1_tests_1_1_tests_1_1_vehicle_logic_tests.html#a4bac6de05610112073c52355da53864e", null ]
];