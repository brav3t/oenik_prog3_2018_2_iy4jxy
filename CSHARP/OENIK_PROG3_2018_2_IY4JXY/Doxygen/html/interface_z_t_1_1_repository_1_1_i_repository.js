var interface_z_t_1_1_repository_1_1_i_repository =
[
    [ "GetAll", "interface_z_t_1_1_repository_1_1_i_repository.html#ab8cb4a0f7366f8bf865e6daad54c221e", null ],
    [ "GetItemById", "interface_z_t_1_1_repository_1_1_i_repository.html#aef33665ba3b6df9f7b75c0195c20f610", null ],
    [ "Insert", "interface_z_t_1_1_repository_1_1_i_repository.html#a019f802a73d3a39678e05814c0ddf9cf", null ],
    [ "Remove", "interface_z_t_1_1_repository_1_1_i_repository.html#a41a7d35336ab51e554bc4d8fbc800440", null ],
    [ "Update", "interface_z_t_1_1_repository_1_1_i_repository.html#a7d43ee2f4168242fae60660a515df819", null ]
];