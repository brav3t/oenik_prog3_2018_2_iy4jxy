var interface_z_t_1_1_logic_1_1_i_logic =
[
    [ "AddNewItem", "interface_z_t_1_1_logic_1_1_i_logic.html#a375b7786dcffc8561c56ba6207e58190", null ],
    [ "ListItems", "interface_z_t_1_1_logic_1_1_i_logic.html#a522f6e232d71ac420fcb7cacea7fba63", null ],
    [ "RemoveItem", "interface_z_t_1_1_logic_1_1_i_logic.html#af62729c0de1b783c66881fd466a97da9", null ],
    [ "UpdateItem", "interface_z_t_1_1_logic_1_1_i_logic.html#af27709f497bb996e380a761527745348", null ]
];