var namespace_z_t_1_1_logic_1_1_logics =
[
    [ "AddressLogic", "class_z_t_1_1_logic_1_1_logics_1_1_address_logic.html", "class_z_t_1_1_logic_1_1_logics_1_1_address_logic" ],
    [ "BusinessLogic", "class_z_t_1_1_logic_1_1_logics_1_1_business_logic.html", "class_z_t_1_1_logic_1_1_logics_1_1_business_logic" ],
    [ "CrudLogic", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic.html", "class_z_t_1_1_logic_1_1_logics_1_1_crud_logic" ],
    [ "DriversLogic", "class_z_t_1_1_logic_1_1_logics_1_1_drivers_logic.html", "class_z_t_1_1_logic_1_1_logics_1_1_drivers_logic" ],
    [ "PassengersLogic", "class_z_t_1_1_logic_1_1_logics_1_1_passengers_logic.html", "class_z_t_1_1_logic_1_1_logics_1_1_passengers_logic" ],
    [ "TransportsLogic", "class_z_t_1_1_logic_1_1_logics_1_1_transports_logic.html", "class_z_t_1_1_logic_1_1_logics_1_1_transports_logic" ],
    [ "VehiclesLogic", "class_z_t_1_1_logic_1_1_logics_1_1_vehicles_logic.html", "class_z_t_1_1_logic_1_1_logics_1_1_vehicles_logic" ]
];