
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZT.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ADDRESS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ADDRESS()
        {
            this.TRANSPORTS = new HashSet<TRANSPORT>();
            this.TRANSPORTS1 = new HashSet<TRANSPORT>();
            this.PASSENGERS = new HashSet<PASSENGER>();
        }
    
        public int ID_ADDRESS { get; set; }
        public int ZIP_CODE { get; set; }
        public string CITY { get; set; }
        public string STREET_NAME { get; set; }
        public string STREET_TYPE { get; set; }
        public string STREET_NUMBER { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TRANSPORT> TRANSPORTS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TRANSPORT> TRANSPORTS1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PASSENGER> PASSENGERS { get; set; }
    }
}
