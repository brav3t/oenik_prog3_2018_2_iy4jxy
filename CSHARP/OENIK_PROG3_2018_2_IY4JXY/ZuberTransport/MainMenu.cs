﻿// <copyright file="MainMenu.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZuberTransport
{
    using EasyConsole;
    using ZT.Logic.Logics;
    using ZuberTransport.MenuPages;

    /// <summary>
    /// Initializes the main menu
    /// </summary>
    public class MainMenu : Program
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        /// <param name="al">The address logic</param>
        /// <param name="dl">The drivers logic</param>
        /// <param name="pl">The passengers logic</param>
        /// <param name="tl">The transports logic</param>
        /// <param name="vl">The Vehicles logic</param>
        /// <param name="bl">The business logic</param>
        public MainMenu(AddressLogic al, DriversLogic dl, PassengersLogic pl, TransportsLogic tl, VehiclesLogic vl, BusinessLogic bl)
            : base("Zuber Transport program", breadcrumbHeader: true)
        {
            this.AddPage(new MainPage(this));
            this.AddPage(new TransportsPage(this, tl));
            this.AddPage(new PassengersPage(this, pl));
            this.AddPage(new AddressesPage(this, al));
            this.AddPage(new DriversPage(this, dl));
            this.AddPage(new VehiclesPage(this, vl));
            this.AddPage(new BusinessPage(this, bl, dl));
            this.AddPage(new ImportDataPage(this));

            this.SetPage<MainPage>();
        }
    }
}
