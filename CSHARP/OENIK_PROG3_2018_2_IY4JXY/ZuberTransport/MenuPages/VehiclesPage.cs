﻿// <copyright file="VehiclesPage.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZuberTransport.MenuPages
{
    using System;
    using EasyConsole;
    using ZT.Logic;

    /// <summary>
    /// Menu item with vehicles table related options
    /// </summary>
    public class VehiclesPage : TemplatePage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VehiclesPage"/> class.
        /// </summary>
        /// <param name="program">A menu page</param>
        /// <param name="bl">The business logic</param>
        public VehiclesPage(Program program, ILogic bl)
            : base("Vehicles", program, bl)
        {
        }

        /// <inheritdoc/>
        protected override void AddSelected()
        {
            Output.WriteLine(ConsoleColor.Red, "ADD NEW VEHICLE");

            string[] itemProperties = new string[]
            {
                Input.ReadString("BRAND:"),
                Input.ReadString("MODEL:"),
                Input.ReadString("COLOR:"),
                Input.ReadInt("AGE GROUP (1980-2100):", 1980, 2100).ToString(),
                Input.ReadInt("SPEEDOMETER STATE:", 0, 500000).ToString()
            };

            Output.WriteLine(ConsoleColor.Red, this.BL.AddNewItem(itemProperties));

            this.RefreshPage();
        }

        /// <inheritdoc/>
        protected override void ModifySelected()
        {
            Output.WriteLine(ConsoleColor.Red, "MODIFY VEHICLE");

            Console.Write("ID: ");
            int id = Input.ReadInt();

            Output.WriteLine(ConsoleColor.Yellow, "BRAND=1, MODEL=2, COLOR=3, AGE_GROUP=4, SPEEDOMETER_STATE=5");
            int column = Input.ReadInt("COLUMN:", 1, 5);

            string newData = " ";

            switch (column)
            {
                case 1:
                    newData = Input.ReadString("NEW BRAND:");
                    break;
                case 2:
                    newData = Input.ReadString("NEW MODEL:");
                    break;
                case 3:
                    newData = Input.ReadString("NEW COLOR:");
                    break;
                case 4:
                    newData = Input.ReadInt("NEW AGE GROUP (1980-2100):", 1980, 2100).ToString();
                    break;
                case 5:
                    newData = Input.ReadInt("NEW SPEEDOMETER STATE:", 0, 500000).ToString();
                    break;
                default:
                    break;
            }

            Output.WriteLine(ConsoleColor.Red, this.BL.UpdateItem(id, column, newData));

            this.RefreshPage();
        }

        /// <inheritdoc/>
        protected override void RefreshPage()
        {
            Input.ReadString("Press [Enter]");
            this.Program.NavigateTo<VehiclesPage>();
        }
    }
}