﻿// <copyright file="BusinessPage.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZuberTransport.MenuPages
{
    using System;
    using EasyConsole;
    using ZT.Logic.Logics;

    /// <summary>
    /// Menu item with business related options
    /// </summary>
    public class BusinessPage : Page
    {
        private readonly BusinessLogic bl;
        private readonly DriversLogic dl;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessPage"/> class.
        /// </summary>
        /// <param name="program">main menu's program</param>
        /// <param name="bl">business logic</param>
        /// <param name="dl">drivers logic</param>
        public BusinessPage(Program program, BusinessLogic bl, DriversLogic dl)
            : base("Business methods", program)
        {
            this.bl = bl;
            this.dl = dl;
        }

        /// <summary>
        /// Displays page header an menu
        /// </summary>
        public override void Display()
        {
            base.Display();

            var menu = new Menu()
                .Add("View Transports", () => this.DisplayTransportsView())
                .Add("Get top 3 customers", () => this.DisplayTop3Customer())
                .Add("Get top 3 drivers by point", () => this.DisplayTop3DriversByPoints())
                .Add("Get top 3 drivers by drives", () => this.DisplayTop3DriversByDrives())
                .Add("Give driver points", () => this.DisplayGiveDriverPoints())
                .Add("Get average car age", () => this.DisplayAverageCarAge())
                .Add("Go back", () => this.Program.NavigateBack());
            menu.Display();
        }

        private void DisplayTransportsView()
        {
            Output.WriteLine(ConsoleColor.Red, "TRANSPORTS SO FAR:");
            Output.WriteLine(this.bl.CreateTransportsView());
            this.RefreshPage();
        }

        private void DisplayTop3Customer()
        {
            Output.WriteLine(ConsoleColor.Red, "TOP 3 CUSTOMERS:");
            Output.WriteLine(this.bl.GetTop3Customers());
            this.RefreshPage();
        }

        private void DisplayTop3DriversByPoints()
        {
            Output.WriteLine(ConsoleColor.Red, "TOP 3 DRIVERS BY POINTS:");
            Output.WriteLine(this.bl.GetTop3DriversByPoint());
            this.RefreshPage();
        }

        private void DisplayTop3DriversByDrives()
        {
            Output.WriteLine(ConsoleColor.Red, "TOP 3 DRIVERS BY DRIVES:");
            Output.WriteLine(this.bl.GetTop3DriversByDrives());
            this.RefreshPage();
        }

        private void DisplayGiveDriverPoints()
        {
            Output.WriteLine(this.dl.ListItems());

            Output.WriteLine(ConsoleColor.Red, "GIVE DRIVER POINTS");
            Console.Write("ID: ");
            Output.WriteLine(ConsoleColor.Red, this.bl.GiveDriverPoints(
                Input.ReadInt(),
                Input.ReadInt("How many points do you want to add? ", 1, 10)));

            this.RefreshPage();
        }

        private void DisplayAverageCarAge()
        {
            Output.WriteLine(ConsoleColor.Red, "AVERAGE CAR AGE AT ZUBER");
            Output.WriteLine(this.bl.GetAverageCarAge());
            this.RefreshPage();
        }

        private void RefreshPage()
        {
            Input.ReadString("Press [Enter]");
            this.Program.NavigateTo<BusinessPage>();
        }
    }
}
