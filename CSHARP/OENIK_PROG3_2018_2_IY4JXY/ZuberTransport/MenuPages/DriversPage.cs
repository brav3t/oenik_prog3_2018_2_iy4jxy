﻿// <copyright file="DriversPage.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZuberTransport.MenuPages
{
    using System;
    using EasyConsole;
    using ZT.Logic;

    /// <summary>
    /// Menu item with drivers table related options
    /// </summary>
    public class DriversPage : TemplatePage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DriversPage"/> class.
        /// </summary>
        /// <param name="program">A menu page</param>
        /// <param name="bl">The business logic</param>
        public DriversPage(Program program, ILogic bl)
            : base("Drivers", program, bl)
        {
        }

        /// <inheritdoc/>
        protected override void AddSelected()
        {
            Output.WriteLine(ConsoleColor.Red, "ADD NEW DRIVER");

            string[] itemProperties = new string[]
            {
                Input.ReadString("Last name:"),
                Input.ReadString("First Name:"),
                DateTime.Now.ToString(),
                Input.ReadInt("Experience:", 3, 50).ToString(),
                "0"
            };

            Output.WriteLine(ConsoleColor.Red, this.BL.AddNewItem(itemProperties));

            this.RefreshPage();
        }

        /// <inheritdoc/>
        protected override void ModifySelected()
        {
            Output.WriteLine(ConsoleColor.Red, "MODIFY DRIVER");

            Console.Write("ID: ");
            int id = Input.ReadInt();

            Output.WriteLine(ConsoleColor.Yellow, "LAST_NAME = 1, FIRST_NAME = 2, STARTDATE_OF_WORK=3, EXPERIENCE=4, POINTS=5");
            int column = Input.ReadInt("COLUMN:", 1, 5);

            string newData = " ";

            switch (column)
            {
                case 1:
                    newData = Input.ReadString("NEW LAST NAME");
                    break;
                case 2:
                    newData = Input.ReadString("NEW FIRST NAME");
                    break;
                case 3:
                    newData = Input.ReadString("NEW STARTDATE OF WORK (yyyy.MM.dd):");
                    break;
                case 4:
                    newData = Input.ReadInt("NEW EXPERIENCE:", 3, 50).ToString();
                    break;
                case 5:
                    newData = Input.ReadInt("NEW POINTS:", 1, 1000).ToString();
                    break;
                default:
                    break;
            }

            Output.WriteLine(ConsoleColor.Red, this.BL.UpdateItem(id, column, newData));

            this.RefreshPage();
        }

        /// <inheritdoc/>
        protected override void RefreshPage()
        {
            Input.ReadString("Press [Enter]");
            this.Program.NavigateTo<DriversPage>();
        }
    }
}