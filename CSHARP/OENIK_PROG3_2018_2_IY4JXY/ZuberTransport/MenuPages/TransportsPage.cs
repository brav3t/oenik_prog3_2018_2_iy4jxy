﻿// <copyright file="TransportsPage.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZuberTransport.MenuPages
{
    using System;
    using EasyConsole;
    using ZT.Logic;

    /// <summary>
    /// Menu item with transports table related options
    /// </summary>
    public class TransportsPage : TemplatePage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransportsPage"/> class.
        /// </summary>
        /// <param name="program">A menu page</param>
        /// <param name="bl">The business logic</param>
        public TransportsPage(Program program, ILogic bl)
            : base("Transports", program, bl)
        {
        }

        /// <inheritdoc/>
        protected override void AddSelected()
        {
            Output.WriteLine(ConsoleColor.Red, "ADD NEW TRANSPORT");

            int transportTime = Input.ReadInt("How many MINUTES does the transport take?", 1, 2500);
            DateTime transportEndTime = DateTime.Now;
            DateTime transportStartTime = transportEndTime.Add(new TimeSpan(0, -transportTime, 0));

            string[] itemProperties = new string[]
            {
               Input.ReadInt("DRIVER ID:", 1, 99999).ToString(),
               Input.ReadInt("PASSENGER ID:", 1, 99999).ToString(),
               transportStartTime.ToString(),
               Input.ReadInt("DEPARTURE ADDRESS ID:", 1, 99999).ToString(),
               transportEndTime.ToString(),
               Input.ReadInt("DESTINATION ADDRESS ID:", 1, 99999).ToString(),
               Input.ReadInt("VEHICLE ID", 1, 99999).ToString()
        };

            Output.WriteLine(ConsoleColor.Red, this.BL.AddNewItem(itemProperties));

            this.RefreshPage();
        }

        /// <inheritdoc/>
        protected override void ModifySelected()
        {
            Output.WriteLine(ConsoleColor.Red, "MODIFY TRANSPORT");

            Console.Write("ID: ");
            int id = Input.ReadInt();

            Output.WriteLine(ConsoleColor.Yellow, "DRIVER_ID=1, PASSENGER_ID=2, DEPARTURE_TIME=3, DEPARTURE_ADDRESS_ID=4, ARRIVE_TIME=5, DESTINATION_ADDR_ID=6, VEHICLE_ID=7");
            int column = Input.ReadInt("COLUMN:", 1, 7);

            string newData = " ";

            switch (column)
            {
                case 1:
                    newData = Input.ReadInt("NEW DRIVER ID:", 1, 99999).ToString();
                    break;
                case 2:
                    newData = Input.ReadInt("NEW PASSENGER ID:", 1, 999999).ToString();
                    break;
                case 3:
                    newData = Input.ReadString("NEW DEPARTURE TIME (yyyy.MM.dd - hh:mm):");
                    break;
                case 4:
                    newData = Input.ReadInt("NEW DEPARTURE ADDRESS ID:", 1, 99999).ToString();
                    break;
                case 5:
                    newData = Input.ReadString("NEW ARRIVE TIME (yyyy.MM.dd - hh:mm):");
                    break;
                case 6:
                    newData = Input.ReadInt("NEW DESTINATION ADDRESS ID:", 1, 99999).ToString();
                    break;
                case 7:
                    newData = Input.ReadInt("NEW VEHICLE ID", 1, 99999).ToString();
                    break;
                default:
                    break;
            }

            Output.WriteLine(ConsoleColor.Red, this.BL.UpdateItem(id, column, newData));

            this.RefreshPage();
        }

        /// <inheritdoc/>
        protected override void RefreshPage()
        {
            Input.ReadString("Press [Enter]");
            this.Program.NavigateTo<TransportsPage>();
        }
    }
}