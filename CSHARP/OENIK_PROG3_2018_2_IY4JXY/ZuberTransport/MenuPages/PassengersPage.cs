﻿// <copyright file="PassengersPage.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZuberTransport.MenuPages
{
    using System;
    using EasyConsole;
    using ZT.Logic;

    /// <summary>
    /// Menu item with passengers table related options
    /// </summary>
    public class PassengersPage : TemplatePage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PassengersPage"/> class.
        /// </summary>
        /// <param name="program">A menu page</param>
        /// <param name="bl">The business logic</param>
        public PassengersPage(Program program, ILogic bl)
            : base("Passengers", program, bl)
        {
        }

        /// <inheritdoc/>
        protected override void AddSelected()
        {
            Output.WriteLine(ConsoleColor.Red, "ADD NEW PASSENGER");

            string[] itemProperties = new string[]
            {
                Input.ReadString("LAST NAME:"),
                Input.ReadString("FIRST NAME:"),
                Input.ReadInt("ADDRESS ID:", 1, 99999).ToString()
            };

            Output.WriteLine(ConsoleColor.Red, this.BL.AddNewItem(itemProperties));

            this.RefreshPage();
        }

        /// <inheritdoc/>
        protected override void ModifySelected()
        {
            Output.WriteLine(ConsoleColor.Red, "MODIFY PASSENGER");

            Console.Write("ID: ");
            int id = Input.ReadInt();

            Output.WriteLine(ConsoleColor.Yellow, "LAST_NAME=1, FIRST_NAME=2, ADDRESS_ID=3");
            int column = Input.ReadInt("COLUMN:", 1, 3);

            string newData = " ";

            switch (column)
            {
                case 1:
                    newData = Input.ReadString("LAST NAME:");
                    break;
                case 2:
                    newData = Input.ReadString("FIRST NAME:");
                    break;
                case 3:
                    newData = Input.ReadInt("ADDRESS_ID:", 1, 99999).ToString();
                    break;
                default:
                    break;
            }

            Output.WriteLine(ConsoleColor.Red, this.BL.UpdateItem(id, column, newData));

            this.RefreshPage();
        }

        /// <inheritdoc/>
        protected override void RefreshPage()
        {
            Input.ReadString("Press [Enter]");
            this.Program.NavigateTo<PassengersPage>();
        }
    }
}
