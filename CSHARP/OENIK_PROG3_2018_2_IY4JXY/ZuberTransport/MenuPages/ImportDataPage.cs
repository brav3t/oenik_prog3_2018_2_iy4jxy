﻿// <copyright file="ImportDataPage.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZuberTransport.MenuPages
{
    using EasyConsole;

    /// <summary>
    /// Menu item with java import related options
    /// </summary>
    public class ImportDataPage : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImportDataPage"/> class.
        /// </summary>
        /// <param name="program">A menu page</param>
        public ImportDataPage(Program program)
            : base("Import applicants", program)
        {
        }

        /// <summary>
        /// Display default
        /// </summary>
        public override void Display()
        {
            base.Display();

            Output.WriteLine("Not implemented function\n");

            var menu = new Menu()
                .Add("DISPLAY NEW APPLICANTS", () => this.GetApplicants())
                .Add("Go back",                () => this.Program.NavigateBack());
            menu.Display();
        }

        /// <summary>
        /// Get new applicants from web
        /// </summary>
        private void GetApplicants()
        {
            Output.WriteLine("Not implemented");

            this.RefreshPage();
        }

        /// <summary>
        /// Refreshes current page after function
        /// </summary>
        private void RefreshPage()
        {
            Input.ReadString("Press [Enter]");
            this.Program.NavigateTo<ImportDataPage>();
        }
    }
}