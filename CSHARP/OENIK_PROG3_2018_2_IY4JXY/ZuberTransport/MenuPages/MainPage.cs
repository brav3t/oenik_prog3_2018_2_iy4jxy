﻿// <copyright file="MainPage.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZuberTransport.MenuPages
{
    using EasyConsole;

    /// <summary>
    /// Creates the main main-menu page
    /// </summary>
    public class MainPage : MenuPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainPage"/> class.
        /// </summary>
        /// <param name="program">A menu page</param>
        public MainPage(Program program)
            : base(
                  "ZUBER TRANSPORT - Transports management system",
                  program,
                  new Option("CRUD for TRANSPORTS table", () => program.NavigateTo<TransportsPage>()),
                  new Option("CRUD for PASSENGERS table", () => program.NavigateTo<PassengersPage>()),
                  new Option("CRUD for ADDRESSES  table", () => program.NavigateTo<AddressesPage>()),
                  new Option("CRUD for DRIVERS    table", () => program.NavigateTo<DriversPage>()),
                  new Option("CRUD for VEHICLES   table", () => program.NavigateTo<VehiclesPage>()),
                  new Option("NO CRUD for Business related methods", () => program.NavigateTo<BusinessPage>()),
                  new Option("Get new driver applicants from web (Java/XML)", () => program.NavigateTo<ImportDataPage>()),
                  new Option("Exit program", () => Exit()))
        {
        }

        private static void Exit()
        {
            Output.WriteLine(System.ConsoleColor.Yellow, "Goodbye!");
            return;
        }
    }
}
