﻿// <copyright file="AddressesPage.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZuberTransport.MenuPages
{
    using System;
    using EasyConsole;
    using ZT.Logic;

    /// <summary>
    /// Menu item with address table related options
    /// </summary>
    public class AddressesPage : TemplatePage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddressesPage"/> class.
        /// </summary>
        /// <param name="program">A menu page</param>
        /// <param name="bl">The bussiness logic</param>
        public AddressesPage(Program program, ILogic bl)
            : base("ADDRESS", program, bl)
        {
        }

        /// <inheritdoc/>
        public override void Display()
        {
            base.Display();
        }

        /// <inheritdoc/>
        protected override void AddSelected()
        {
            Output.WriteLine(ConsoleColor.Red, "ADD NEW ADDRESS");

            string[] itemProperties = new string[]
            {
                Input.ReadInt("ZIP CODE:", 1000, 9999).ToString(),
                Input.ReadString("CITY:"),
                Input.ReadString("STREET NAME:"),
                Input.ReadString("STREET TYPE:"),
                Input.ReadString("STREET NUMBER:")
            };

            Output.WriteLine(ConsoleColor.Red, this.BL.AddNewItem(itemProperties));

            this.RefreshPage();
        }

        /// <inheritdoc/>
        protected override void ModifySelected()
        {
            Output.WriteLine(ConsoleColor.Red, "MODIFY ADDRESS");

            Console.Write("ID: ");
            int id = Input.ReadInt();

            Output.WriteLine(ConsoleColor.Yellow, "ZIP_CODE=1, CITY=2, STREET_NAME=3, STREET_TYPE=4, STREET_NUMBER=5");
            int column = Input.ReadInt("COLUMN:", 1, 5);

            string newData = " ";

            switch (column)
            {
                case 1:
                    newData = Input.ReadInt("NEW ZIP_CODE:", 1000, 9999).ToString();
                    break;
                case 2:
                    newData = Input.ReadString("NEW CITY:");
                    break;
                case 3:
                    newData = Input.ReadString("NEW STREET NAME:");
                    break;
                case 4:
                    newData = Input.ReadString("NEW STREET TYPE:");
                    break;
                case 5:
                    newData = Input.ReadString("NEW STREET NUMBER:");
                    break;
                default:
                    break;
            }

            Output.WriteLine(ConsoleColor.Red, this.BL.UpdateItem(id, column, newData));

            this.RefreshPage();
        }

        /// <inheritdoc/>
        protected override void RefreshPage()
        {
            Input.ReadString("Press [Enter]");
            this.Program.NavigateTo<AddressesPage>();
        }
    }
}