﻿// <copyright file="TemplatePage.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZuberTransport.MenuPages
{
    using System;
    using EasyConsole;
    using ZT.Logic;

    /// <summary>
    /// A template page for other pages
    /// </summary>
    public abstract class TemplatePage : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TemplatePage"/> class.
        /// </summary>
        /// <param name="program">A menu page</param>
        /// <param name="bl">Business logic constructor injection</param>
        /// <param name="pageName">A menu page's name</param>
        public TemplatePage(string pageName, Program program, ILogic bl)
            : base(pageName, program)
        {
            this.PageName = pageName;
            this.BL = bl;
        }

        /// <summary>
        /// Gets current page's name
        /// </summary>
        protected string PageName { get; }

        /// <summary>
        /// Gets the bussiness logic
        /// </summary>
        protected ILogic BL { get; }

        /// <summary>
        /// Displays header and menu of the page
        /// </summary>
        public override void Display()
        {
            base.Display();

            Output.WriteLine(this.BL.ListItems());

            var menu = new Menu()
                .Add("INSERT " + this.PageName, () => this.AddSelected())
                .Add("REMOVE " + this.PageName, () => this.RemoveSelected())
                .Add("MODIFY " + this.PageName, () => this.ModifySelected())
                .Add("Go back", () => this.Program.NavigateBack());
            menu.Display();
        }

        /// <summary>
        /// Removes entity from current context menu item
        /// </summary>
        protected void RemoveSelected()
        {
            Output.WriteLine(ConsoleColor.Red, "REMOVE ITEM");

            Console.Write("ID of item: ");
            Output.WriteLine(ConsoleColor.Red, this.BL.RemoveItem(Input.ReadInt()));

            this.RefreshPage();
        }

        /// <summary>
        /// Adds new entitiy to current context menu item
        /// </summary>
        protected abstract void AddSelected();

        /// <summary>
        /// Modifies an entity in current context menu item
        /// </summary>
        protected abstract void ModifySelected();

        /// <summary>
        /// Takes back to selection after query
        /// </summary>
        protected abstract void RefreshPage();
    }
}
