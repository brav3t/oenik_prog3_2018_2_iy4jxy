﻿// <copyright file="Runner.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZuberTransport
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using ZT.Data;
    using ZT.Logic.Logics;
    using ZT.Repository.Repos;

    /// <summary>
    /// Main program starter
    /// </summary>
    public class Runner
    {
        /// <summary>
        /// Creates and starts the main menu
        /// </summary>
        /// <param name="args">Starting attributes of the program</param>
        public static void Main(string[] args)
        {
            ZTEntities zt = new ZTEntities();

            AddressRepo addressRepo = new AddressRepo(zt);
            DriversRepo driversRepo = new DriversRepo(zt);
            PassengersRepo passengersRepo = new PassengersRepo(zt);
            TransportsRepo transportsRepo = new TransportsRepo(zt);
            VehiclesRepo vehiclesRepo = new VehiclesRepo(zt);

            AddressLogic addressLogic = new AddressLogic(addressRepo);
            DriversLogic driversLogic = new DriversLogic(driversRepo);
            PassengersLogic passengersLogic = new PassengersLogic(passengersRepo);
            TransportsLogic transportsLogic = new TransportsLogic(transportsRepo);
            VehiclesLogic vehiclesLogic = new VehiclesLogic(vehiclesRepo);
            BusinessLogic businessLogic = new BusinessLogic(addressRepo, driversRepo, passengersRepo, transportsRepo, vehiclesRepo);

            Console.SetWindowSize(161, 35);
            Console.ForegroundColor = ConsoleColor.Red;

            // "loading querry to memory" for fast first time use
            Task task = Task.Run(() =>
            {
                passengersLogic.ListItems();
            });

            Console.Write("Loading");
            do
            {
                Console.Write('.');
                Thread.Sleep(400);
            }
            while (!task.IsCompleted);

            task.Wait();

            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Gray;

            new MainMenu(addressLogic, driversLogic, passengersLogic, transportsLogic, vehiclesLogic, businessLogic).Run();
        }
    }
}
