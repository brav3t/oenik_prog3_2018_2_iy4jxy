﻿// <copyright file="IRepository.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Repository
{
    using System.Linq;

    /// <summary>
    /// Interface for repository's CRUD methods
    /// </summary>
    /// <typeparam name="T">entitry</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Returns table items
        /// </summary>
        /// <returns>Queryable table data</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Get item by it's id
        /// </summary>
        /// <param name="id">id of item</param>
        /// <returns>Requested item</returns>
        T GetItemById(int id);

        /// <summary>
        /// Insert data into table
        /// </summary>
        /// <param name="entity">new entity to insert</param>
        void Insert(T entity);

        /// <summary>
        /// Delete data from database
        /// </summary>
        /// <param name="entity">entity that to ne deleted</param>
        void Remove(T entity);

        /// <summary>
        /// Update database data
        /// </summary>
        /// <param name="entity"> entity to update</param>
        void Update(T entity);
    }
}
