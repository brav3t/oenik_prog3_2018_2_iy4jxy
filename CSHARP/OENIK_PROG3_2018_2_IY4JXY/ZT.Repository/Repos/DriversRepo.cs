﻿// <copyright file="DriversRepo.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Repository.Repos
{
    using ZT.Data;

    /// <summary>
    /// Repository for drivers table
    /// </summary>
    public class DriversRepo : Repository<DRIVER>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DriversRepo"/> class.
        /// </summary>
        /// <param name="zt">ZT entities</param>
        public DriversRepo(ZTEntities zt)
            : base(zt)
        {
        }
    }
}
