﻿// <copyright file="Repository.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Repository.Repos
{
    using System.Data.Entity;
    using System.Linq;
    using ZT.Data;

    /// <summary>
    /// Generic CRUD methods for repositorys
    /// </summary>
    /// <typeparam name="T">entitry type</typeparam>
    public abstract class Repository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="zt">Zuber Transport entites</param>
        public Repository(ZTEntities zt)
        {
            this.ZT = zt;
            this.DbSet = zt.Set<T>();
        }

        /// <summary>
        /// Gets ZT entities
        /// </summary>
        internal ZTEntities ZT { get; }

        /// <summary>
        /// Gets current table
        /// </summary>
        internal DbSet<T> DbSet { get; }

        /// <inheritdoc/>
        public IQueryable<T> GetAll()
        {
            return this.DbSet.AsQueryable();
        }

        /// <inheritdoc/>
        public T GetItemById(int id)
        {
            return this.DbSet.Find(id);
        }

        /// <inheritdoc/>
        public void Insert(T entity)
        {
            this.DbSet.Add(entity);
            this.ZT.SaveChanges();
        }

        /// <inheritdoc/>
        public void Remove(T entity)
        {
            this.DbSet.Remove(entity);
            this.ZT.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update(T entity)
        {
            this.ZT.Entry(entity).State = EntityState.Modified;
            this.ZT.SaveChanges();
        }
    }
}
