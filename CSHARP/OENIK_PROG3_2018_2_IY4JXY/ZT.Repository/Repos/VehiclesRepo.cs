﻿// <copyright file="VehiclesRepo.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Repository.Repos
{
    using ZT.Data;

    /// <summary>
    /// Repository for vehicles
    /// </summary>
    public class VehiclesRepo : Repository<VEHICLE>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VehiclesRepo"/> class.
        /// </summary>
        /// <param name="zt">ZT entities</param>
        public VehiclesRepo(ZTEntities zt)
            : base(zt)
        {
        }
    }
}
