﻿// <copyright file="TransportsRepo.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Repository.Repos
{
    using ZT.Data;

    /// <summary>
    /// Repository for transports
    /// </summary>
    public class TransportsRepo : Repository<TRANSPORT>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransportsRepo"/> class.
        /// </summary>
        /// <param name="zt">ZT Entities</param>
        public TransportsRepo(ZTEntities zt)
            : base(zt)
        {
        }
    }
}
