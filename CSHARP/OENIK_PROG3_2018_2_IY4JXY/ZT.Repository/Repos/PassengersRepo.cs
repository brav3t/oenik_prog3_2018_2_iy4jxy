﻿// <copyright file="PassengersRepo.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Repository.Repos
{
    using ZT.Data;

    /// <summary>
    /// Repository for passengers table
    /// </summary>
    public class PassengersRepo : Repository<PASSENGER>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PassengersRepo"/> class.
        /// </summary>
        /// <param name="zt">ZT entities</param>
        public PassengersRepo(ZTEntities zt)
            : base(zt)
        {
        }
    }
}
