﻿// <copyright file="AddressRepo.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Repository.Repos
{
    using ZT.Data;

    /// <summary>
    /// Repository for addresses table
    /// </summary>
    public class AddressRepo : Repository<ADDRESS>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddressRepo"/> class.
        /// </summary>
        /// <param name="zt">ZT entities</param>
        public AddressRepo(ZTEntities zt)
            : base(zt)
        {
        }
    }
}
