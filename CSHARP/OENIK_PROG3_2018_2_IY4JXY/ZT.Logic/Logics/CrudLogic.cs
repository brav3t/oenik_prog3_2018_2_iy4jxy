﻿// <copyright file="CrudLogic.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Logics
{
    using System;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using ZT.Repository;

    /// <summary>
    /// Contains generic CRUD methods
    /// </summary>
    /// <typeparam name="T">Type of businesslogic</typeparam>
    public abstract class CrudLogic<T> : ILogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CrudLogic{T}"/> class.
        /// </summary>
        /// <param name="repo">A repository</param>
        public CrudLogic(IRepository<T> repo)
        {
            this.Repo = repo;
        }

        /// <summary>
        /// Gets a repository
        /// </summary>
        internal IRepository<T> Repo { get; }

        /// <inheritdoc/>
        public string ListItems()
        {
            try
            {
                var items = this.Repo.GetAll();

                PropertyInfo[] properties = typeof(T).GetProperties();

                StringBuilder sb = new StringBuilder();

                // header
                foreach (var p in properties)
                {
                    if (p.PropertyType == typeof(int) || p.PropertyType == typeof(string) || p.PropertyType == typeof(DateTime) || p.PropertyType == typeof(int?))
                    {
                        sb.AppendFormat($"{p.Name, -20}");
                    }
                }

                sb.AppendLine();

                // table data
                foreach (var i in items)
                {
                    foreach (var p in properties)
                    {
                        if (p.PropertyType == typeof(int) || p.PropertyType == typeof(string) || p.PropertyType == typeof(int?))
                        {
                            sb.AppendFormat($"{p.GetValue(i), -20}");
                        }
                        else if (p.PropertyType == typeof(DateTime))
                        {
                            sb.AppendFormat($"{p.GetValue(i), -20 :yyyy.MM.dd - HH:mm}");
                        }
                    }

                    sb.AppendLine();
                }

                return sb.ToString();
            }
            catch (Exception e)
            {
                return $"ERROR: {e.GetBaseException().Message}";
            }
        }

        /// <inheritdoc/>
        public string RemoveItem(int id)
        {
            try
            {
                T entity = this.Repo.GetItemById(id);

                this.Repo.Remove(entity);

                return $"ROW WITH ID={id} IS DELETED!";
            }
            catch (ArgumentNullException)
            {
                return $"ERROR: Table contains no row with ID={id}";
            }
            catch (DbUpdateException e)
            {
                e.Entries.Single().Reload();
                return $"ERROR: {e.GetBaseException().Message}";
            }
            catch (Exception e)
            {
                return $"ERROR: {e.GetBaseException().Message}";
            }
        }

        /// <inheritdoc/>
        public string UpdateItem(int id, int column, string newValue)
        {
            try
            {
                T entity = this.Repo.GetItemById(id);

                PropertyInfo[] properties = typeof(T).GetProperties();

                string oldValue = properties[column].GetValue(entity).ToString();
                string columnName = properties[column].Name;

                if (properties[column].PropertyType == typeof(int))
                {
                    properties[column].SetValue(entity, int.Parse(newValue));
                }
                else if (properties[column].PropertyType == typeof(string))
                {
                    properties[column].SetValue(entity, newValue);
                }
                else if (properties[column].PropertyType == typeof(DateTime))
                {
                    properties[column].SetValue(entity, Convert.ToDateTime(newValue));
                }
                else if (properties[column].PropertyType == typeof(int?))
                {
                    properties[column].SetValue(entity, int.Parse(newValue));
                }

                this.Repo.Update(entity);

                return $"ROW WITH ID={id} MODIFIED! {columnName}: NEW VALUE='{newValue}' OLD VALUE='{oldValue}'";
            }
            catch (TargetException)
            {
                return $"ERROR: Table contains no row with ID={id}";
            }
            catch (DbUpdateException e)
            {
                // e.Entries.Single().State = System.Data.Entity.EntityState.Unchanged;
                e.Entries.Single().Reload();

                if (e.GetBaseException().Message.Contains("FK_TRANSPORT_DRIVER"))
                {
                    return $"ERROR: DRIVER_ID is not in DRIVERS table";
                }
                else if (e.GetBaseException().Message.Contains("FK_TRANSPORT_PASSENGER"))
                {
                    return $"ERROR: PASSENGER_ID is not in PASSENGERS table";
                }
                else if (e.GetBaseException().Message.Contains("FK_DEPARTURE_ADDR"))
                {
                    return $"ERROR: departure ADDRESS_ID is not in ADDRESS table";
                }
                else if (e.GetBaseException().Message.Contains("FK_DESTINATION_ADDR"))
                {
                    return $"ERROR: destination ADDRESS_ID is not in ADDRESS table";
                }
                else if (e.GetBaseException().Message.Contains("FK_TRANSPORT_VEHICLE"))
                {
                    return $"ERROR: VEHICLE_ID is not in VEHICLES table";
                }
                else if (e.GetBaseException().Message.Contains("FK_PASSENGER_ADDRESS"))
                {
                    return $"ERROR: ADDRESS_ID is not in ADDRESS table";
                }

                return $"ERROR: {e.GetBaseException().Message}";
            }
            catch (Exception e)
            {
                return $"ERROR: {e.GetBaseException().Message}";
            }
        }

        /// <inheritdoc/>
        public string AddNewItem(string[] newData)
        {
            try
            {
                T entity = Activator.CreateInstance<T>();

                PropertyInfo[] pi = typeof(T).GetProperties();

                for (int i = 0; i < newData.Length; i++)
                {
                    if (pi[i + 1].PropertyType == typeof(int))
                    {
                        pi[i + 1].SetValue(entity, int.Parse(newData[i]));
                    }
                    else if (pi[i + 1].PropertyType == typeof(DateTime))
                    {
                        pi[i + 1].SetValue(entity, Convert.ToDateTime(newData[i]));
                    }
                    else if (pi[i + 1].PropertyType == typeof(string))
                    {
                        pi[i + 1].SetValue(entity, newData[i]);
                    }
                    else if (pi[i + 1].PropertyType == typeof(int?))
                    {
                        pi[i + 1].SetValue(entity, int.Parse(newData[i]));
                    }
                }

                this.Repo.Insert(entity);
                return "NEW ITEM ADDED!";
            }
            catch (DbUpdateException e)
            {
                if (e.GetBaseException().Message.Contains("FK_TRANSPORT_DRIVER"))
                {
                    return $"ERROR: DRIVER_ID is not in DRIVERS table";
                }
                else if (e.GetBaseException().Message.Contains("FK_TRANSPORT_PASSENGER"))
                {
                    return $"ERROR: PASSENGER_ID is not in PASSENGERS table";
                }
                else if (e.GetBaseException().Message.Contains("FK_DEPARTURE_ADDR"))
                {
                    return $"ERROR: departure ADDRESS_ID is not in ADDRESS table";
                }
                else if (e.GetBaseException().Message.Contains("FK_DESTINATION_ADDR"))
                {
                    return $"ERROR: destination ADDRESS_ID is not in ADDRESS table";
                }
                else if (e.GetBaseException().Message.Contains("FK_TRANSPORT_VEHICLE"))
                {
                    return $"ERROR: VEHICLE_ID is not in VEHICLES table";
                }
                else if (e.GetBaseException().Message.Contains("FK_PASSENGER_ADDRESS"))
                {
                    return $"ERROR: ADDRESS_ID is not in ADDRESS table";
                }

                return $"ERROR: {e.GetBaseException().Message}";
            }
            catch (Exception e)
            {
                return $"ERROR: {e.GetBaseException().Message}";
            }
        }
    }
}
