﻿// <copyright file="PassengersLogic.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Logics
{
    using ZT.Data;
    using ZT.Repository;

    /// <summary>
    /// Drivers table related CRUD logic
    /// </summary>
    public class PassengersLogic : CrudLogic<PASSENGER>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PassengersLogic"/> class.
        /// </summary>
        /// <param name="repo">repository of passengers table</param>
        public PassengersLogic(IRepository<PASSENGER> repo)
            : base(repo)
        {
        }
    }
}
