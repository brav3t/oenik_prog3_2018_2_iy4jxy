﻿// <copyright file="VehiclesLogic.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Logics
{
    using ZT.Data;
    using ZT.Repository;

    /// <summary>
    /// Vehicles table related CRUD logic
    /// </summary>
    public class VehiclesLogic : CrudLogic<VEHICLE>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VehiclesLogic"/> class.
        /// </summary>
        /// <param name="repo">repository of vehicles table</param>
        public VehiclesLogic(IRepository<VEHICLE> repo)
            : base(repo)
        {
        }
    }
}
