﻿// <copyright file="DriversLogic.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Logics
{
    using ZT.Data;
    using ZT.Repository;

    /// <summary>
    /// Drivers table related CRUD logic
    /// </summary>
    public class DriversLogic : CrudLogic<DRIVER>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DriversLogic"/> class.
        /// </summary>
        /// <param name="repo">repository of drivers table</param>
        public DriversLogic(IRepository<DRIVER> repo)
            : base(repo)
        {
        }
    }
}
