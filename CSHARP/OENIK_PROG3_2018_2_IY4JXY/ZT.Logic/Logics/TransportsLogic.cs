﻿// <copyright file="TransportsLogic.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Logics
{
    using ZT.Data;
    using ZT.Repository;

    /// <summary>
    /// Transports table related CRUD logic
    /// </summary>
    public class TransportsLogic : CrudLogic<TRANSPORT>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransportsLogic"/> class.
        /// </summary>
        /// <param name="repo">repository of transports table</param>
        public TransportsLogic(IRepository<TRANSPORT> repo)
            : base(repo)
        {
        }
    }
}
