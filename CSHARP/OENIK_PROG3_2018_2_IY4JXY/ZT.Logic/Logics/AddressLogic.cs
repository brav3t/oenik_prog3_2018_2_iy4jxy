﻿// <copyright file="AddressLogic.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Logics
{
    using ZT.Data;
    using ZT.Repository;

    /// <summary>
    /// Address table related CRUD logic
    /// </summary>
    public class AddressLogic : CrudLogic<ADDRESS>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddressLogic"/> class.
        /// </summary>
        /// <param name="repo">repository of addresses table</param>
        public AddressLogic(IRepository<ADDRESS> repo)
            : base(repo)
        {
        }
    }
}
