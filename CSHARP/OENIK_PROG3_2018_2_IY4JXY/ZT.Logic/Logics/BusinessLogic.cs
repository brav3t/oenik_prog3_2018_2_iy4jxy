﻿// <copyright file="BusinessLogic.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Logics
{
    using System;
    using System.Linq;
    using System.Text;
    using ZT.Data;
    using ZT.Repository;

    /// <summary>
    /// Contains non CRUD methods
    /// </summary>
    public class BusinessLogic
    {
        private readonly IRepository<ADDRESS> addressesRepo;
        private readonly IRepository<DRIVER> driversRepo;
        private readonly IRepository<PASSENGER> passengersRepo;
        private readonly IRepository<TRANSPORT> transportsRepo;
        private readonly IRepository<VEHICLE> vehiclesRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessLogic"/> class.
        /// </summary>
        /// <param name="ar">address related repository</param>
        /// <param name="dr">drivers related repository</param>
        /// <param name="pr">passengers related repository</param>
        /// <param name="tr">transports related repository</param>
        /// <param name="vr">vehicles related repository</param>
        public BusinessLogic(IRepository<ADDRESS> ar, IRepository<DRIVER> dr, IRepository<PASSENGER> pr, IRepository<TRANSPORT> tr, IRepository<VEHICLE> vr)
        {
            this.addressesRepo = ar;
            this.driversRepo = dr;
            this.passengersRepo = pr;
            this.transportsRepo = tr;
            this.vehiclesRepo = vr;
        }

        /// <summary>
        /// Creates a user friendly view of transports
        /// </summary>
        /// <returns>string contains easily readable table data</returns>
        public string CreateTransportsView()
        {
            try
            {
                var tview =
                    (from t in this.transportsRepo.GetAll()
                     join d in this.driversRepo.GetAll() on t.DRIVER_ID equals d.ID_DRIVER
                     join p in this.passengersRepo.GetAll() on t.PASSENGER_ID equals p.ID_PASSENGER
                     join a1 in this.addressesRepo.GetAll() on t.DEPARTURE_ADDR_ID equals a1.ID_ADDRESS
                     join a2 in this.addressesRepo.GetAll() on t.DESTINATION_ADDR_ID equals a2.ID_ADDRESS
                     join v in this.vehiclesRepo.GetAll() on t.VEHICLE_ID equals v.ID_VEHICLE
                     select new
                     {
                         id = t.ID_TRANSPORT,
                         drivLName = d.LAST_NAME,
                         drivFNAME = d.FIRST_NAME,
                         passLName = p.LAST_NAME,
                         passFNAME = p.FIRST_NAME,
                         depTime = t.DEPARTURE_TIME,
                         depAddrZIP = a1.ZIP_CODE,
                         depAddrCITY = a1.CITY,
                         depAddrStrNAME = a1.STREET_NAME,
                         depAddrStrTYPE = a1.STREET_TYPE,
                         depAddrStrNUM = a1.STREET_NUMBER,
                         arriveTime = t.ARRIVE_TIME,
                         destAddrZIP = a2.ZIP_CODE,
                         destAddrCITY = a2.CITY,
                         destAddrStrNAME = a2.STREET_NAME,
                         destAddrStrTYPE = a2.STREET_TYPE,
                         destAddrStrNUM = a2.STREET_NUMBER,
                         vehBRAND = v.BRAND,
                         vehMODEL = v.MODELL,
                         vehCOLOR = v.COLOR,
                         vehAGE = v.AGE_GROUP
                     }).AsQueryable();

                StringBuilder sb = new StringBuilder();

                foreach (var t in tview)
                {
                    sb.AppendLine("---------------------------------------------------");
                    sb.AppendFormat($"Fuvar azonosító = {t.id}");
                    sb.AppendLine();
                    sb.AppendFormat($"Sofőr = {t.drivLName} {t.drivFNAME}");
                    sb.AppendLine();
                    sb.AppendFormat($"Utas = {t.passLName} {t.passFNAME}");
                    sb.AppendLine();
                    sb.AppendFormat($"Indulási idő = {t.depTime}");
                    sb.AppendLine();
                    sb.AppendFormat($"Érkezési idő = {t.arriveTime}");
                    sb.AppendLine();
                    sb.AppendFormat($"Indulási cím = {t.destAddrZIP} {t.destAddrCITY}, {t.destAddrStrNAME} {t.destAddrStrTYPE} {t.destAddrStrNUM}");
                    sb.AppendLine();
                    sb.AppendFormat($"Érkezési cím = {t.depAddrZIP} {t.depAddrCITY}, {t.depAddrStrNAME} {t.depAddrStrTYPE} {t.depAddrStrNUM}");
                    sb.AppendLine();
                    sb.AppendFormat($"Jármű typusa = {t.vehBRAND} {t.vehMODEL} ({t.vehAGE}, {t.vehCOLOR})");
                    sb.AppendLine();
                }

                return sb.ToString();
            }
            catch (Exception e)
            {
                return $"ERROR: {e.GetBaseException().Message}";
            }
        }

        /// <summary>
        /// Gets top 3 customers
        /// </summary>
        /// <returns>string with the names of the top three customers</returns>
        public string GetTop3Customers()
        {
            try
            {
                var top3Customers =
                (from t in this.transportsRepo.GetAll()
                 group t by t.PASSENGER_ID into g
                 join p in this.passengersRepo.GetAll() on g.Key equals p.ID_PASSENGER
                 orderby g.Count() descending
                 select new { p.LAST_NAME, p.FIRST_NAME, COUNT = g.Count() }).Take(3);

                StringBuilder sb = new StringBuilder();

                int db = 1;

                foreach (var c in top3Customers)
                {
                    sb.AppendFormat($"{db++}. {c.LAST_NAME} {c.FIRST_NAME} --> {c.COUNT}x vette igénybe a szolgáltatásainkat.");
                    sb.AppendLine();
                }

                return sb.ToString();
            }
            catch (Exception e)
            {
                return $"ERROR: {e.GetBaseException().Message}";
            }
        }

        /// <summary>
        /// Gets top 3 drivers with most points
        /// </summary>
        /// <returns>string contains drivers name and points</returns>
        public string GetTop3DriversByPoint()
        {
            try
            {
                var top3drivers =
                (from d in this.driversRepo.GetAll()
                 orderby d.POINTS descending
                 select d).Take(3);

                StringBuilder sb = new StringBuilder();

                int db = 1;

                foreach (var d in top3drivers)
                {
                    sb.AppendFormat($"{db++}. {d.LAST_NAME} {d.FIRST_NAME} --> POINTS={d.POINTS}");
                    sb.AppendLine();
                }

                return sb.ToString();
            }
            catch (Exception e)
            {
                return $"ERROR: {e.GetBaseException().Message}";
            }
        }

        /// <summary>
        /// Get top 3 drivers with most drives
        /// </summary>
        /// <returns>string contains driver name and count of drives</returns>
        public string GetTop3DriversByDrives()
        {
            try
            {
                var top3Drivers =
                (from t in this.transportsRepo.GetAll()
                 group t by t.PASSENGER_ID into g
                 join d in this.driversRepo.GetAll() on g.Key equals d.ID_DRIVER
                 orderby g.Count() descending
                 select new { d.LAST_NAME, d.FIRST_NAME, COUNT = g.Count() }).Take(3);

                StringBuilder sb = new StringBuilder();

                int db = 1;

                foreach (var d in top3Drivers)
                {
                    sb.AppendFormat($"{db++}. {d.LAST_NAME} {d.FIRST_NAME} --> {d.COUNT}x vitt el utasokat.");
                    sb.AppendLine();
                }

                return sb.ToString();
            }
            catch (Exception e)
            {
                return $"ERROR: {e.GetBaseException().Message}";
            }
        }

        /// <summary>
        /// Gives points to given driver
        /// </summary>
        /// <param name="driverID">id of driver</param>
        /// <param name="pointsToAdd">points to add</param>
        /// <returns>message that the points were added</returns>
        public string GiveDriverPoints(int driverID, int pointsToAdd)
        {
            try
            {
                DRIVER driver = this.driversRepo.GetItemById(driverID);

                int oldPoints = driver.POINTS;
                driver.POINTS += pointsToAdd;
                this.driversRepo.Update(driver);

                return $"ID={driver.ID_DRIVER} NAME={driver.LAST_NAME} {driver.FIRST_NAME} --> NEW POINTS={driver.POINTS}, OLD POINTS={oldPoints}";
            }
            catch (NullReferenceException)
            {
                return $"ERROR: NO DRIVER WITH ID={driverID}!";
            }
            catch (Exception e)
            {
                return $"ERROR: {e.GetBaseException().Message}";
            }
        }

        /// <summary>
        /// Returns the average of cars that are in the Zuber fleet
        /// </summary>
        /// <returns>average of cars in fleet</returns>
        public string GetAverageCarAge()
        {
            try
            {
                var avg = (from c in this.vehiclesRepo.GetAll()
                        select c.AGE_GROUP).Average();

                return $"AVG={avg, 0:F}";
            }
            catch (Exception e)
            {
                return $"ERROR: {e.GetBaseException().Message}";
            }
        }
    }
}
