﻿// <copyright file="ILogic.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic
{
    /// <summary>
    /// Interface for generic CRUD methods
    /// </summary>
    public interface ILogic
    {
        // CRUD methods

        /// <summary>
        /// List entity items
        /// </summary>
        /// <returns>entity's formated string representations</returns>
        string ListItems();

        /// <summary>
        /// Removes item from the current table by user inputed id
        /// </summary>
        /// <param name="id">user inputed id</param>
        /// <returns>remove method result message</returns>
        string RemoveItem(int id);

        /// <summary>
        /// Updates item in table by user inputed new data
        /// </summary>
        /// <param name="id">id of item that needs to be modified</param>
        /// <param name="column">column of table that needs to be modified</param>
        /// <param name="newValue">new data</param>
        /// <returns>update method result message</returns>
        string UpdateItem(int id, int column, string newValue);

        /// <summary>
        /// Creates a new item from user input and sends it to the repository to insert it into the database
        /// </summary>
        /// <param name="newData">properties of new item</param>
        /// <returns>database insertion result message</returns>
        string AddNewItem(string[] newData);
    }
}
