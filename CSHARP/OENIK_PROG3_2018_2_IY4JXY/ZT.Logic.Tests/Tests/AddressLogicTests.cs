﻿// <copyright file="AddressLogicTests.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Tests.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Moq;
    using NUnit.Framework;
    using ZT.Data;
    using ZT.Logic.Logics;
    using ZT.Repository;

    /// <summary>
    /// Addresses table CRUD logic unit tests
    /// </summary>
    [TestFixture]
    public class AddressLogicTests
    {
        private Mock<IRepository<ADDRESS>> addressRepoMock;
        private AddressLogic addressLogic;

        /// <summary>
        /// Gets three testCaseData for address logic methods
        /// </summary>
        public static IEnumerable<TestCaseData> ThreeAddressTestCaseData
        {
            get
            {
                List<TestCaseData> testCases = new List<TestCaseData>()
                {
                    new TestCaseData((object)new string[] { "4000", "város4", "utca4", "utcatípus4", "házszám4" }),
                    new TestCaseData((object)new string[] { "5000", "város5", "utca5", "utcatípus5", "házszám5" }),
                    new TestCaseData((object)new string[] { "6000", "város6", "utca6", "utcatípus6", "házszám6" }),
                };
                return testCases;
            }
        }

        /// <summary>
        /// Setup mock for address logic testing
        /// </summary>
        [SetUp]
        public void SetupAddressRepo()
        {
            this.addressRepoMock = new Mock<IRepository<ADDRESS>>();
            List<ADDRESS> addressList = new List<ADDRESS>()
            {
                new ADDRESS() { ID_ADDRESS = 1, ZIP_CODE = 1000, CITY = "város1", STREET_NAME = "utca1", STREET_TYPE = "utcatípus1", STREET_NUMBER = "házszám1" },
                new ADDRESS() { ID_ADDRESS = 2, ZIP_CODE = 2000, CITY = "város2", STREET_NAME = "utca2", STREET_TYPE = "utcatípus2", STREET_NUMBER = "házszám2" },
                new ADDRESS() { ID_ADDRESS = 3, ZIP_CODE = 3000, CITY = "város3", STREET_NAME = "utca3", STREET_TYPE = "utcatípus3", STREET_NUMBER = "házszám3" }
            };

            this.addressRepoMock
                .Setup(repo => repo.GetAll())
                .Returns(addressList.AsQueryable());

            this.addressRepoMock
                .Setup(repo => repo.Insert(It.IsAny<ADDRESS>()))
                .Callback<ADDRESS>(a => addressList.Add(a));

            this.addressRepoMock
                .Setup(repo => repo.Remove(It.IsAny<ADDRESS>()))
                .Callback<ADDRESS>(a => addressList.Remove(a));

            this.addressLogic = new AddressLogic(this.addressRepoMock.Object);
        }

        /// <summary>
        /// AddNewItem() method adds only one address
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "1000", "város1", "utca1", "utcatípus1", "utcaszám1" })]
        public void AddNewItem_Adds_Only_One_Address_To_Repository(string[] newData)
        {
            // Arrange

            // Act
            this.addressLogic.AddNewItem(newData);

            // Assert
            Assert.That(this.addressRepoMock.Object.GetAll().Count(), Is.EqualTo(4));
        }

        /// <summary>
        /// AddNewItem() returns correct string
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "1000", "város1", "utca1", "utcatípus1", "utcaszám1" })]
        public void AddNewItem_Successfull_Insert_Returns_Correct_String_(string[] newData)
        {
            // Arrange

            // Act, Assert
            Assert.That(this.addressLogic.AddNewItem(newData), Is.EqualTo("NEW ITEM ADDED!"));
        }

        /// <summary>
        /// AddNewItem() method adds correct address to repository
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "1000", "város1", "utca1", "utcatípus1", "utcaszám1" })]
        public void AddNewItem_If_Exception_Thrown_Returns_Correct_String(string[] newData)
        {
            // Arrange
            this.addressRepoMock
                .Setup(x => x.Insert(It.IsAny<ADDRESS>()))
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.addressLogic.AddNewItem(newData).StartsWith("ERROR: "), Is.True);
        }

        /// <summary>
        /// AddNewItem() method adds correct address to repository
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCaseSource("ThreeAddressTestCaseData")]
        public void AddNewItem_Adds_Correct_Address_To_AddressRepo(string[] newData)
        {
            // Arrange

            // Act
            this.addressLogic.AddNewItem(newData);

            // Assert
            Assert.That(this.addressRepoMock.Object.GetAll().Last().ZIP_CODE,      Is.EqualTo(int.Parse(newData[0])));
            Assert.That(this.addressRepoMock.Object.GetAll().Last().CITY,          Is.EqualTo(newData[1]));
            Assert.That(this.addressRepoMock.Object.GetAll().Last().STREET_NAME,   Is.EqualTo(newData[2]));
            Assert.That(this.addressRepoMock.Object.GetAll().Last().STREET_TYPE,   Is.EqualTo(newData[3]));
            Assert.That(this.addressRepoMock.Object.GetAll().Last().STREET_NUMBER, Is.EqualTo(newData[4]));
        }

        /// <summary>
        /// RemoveItem() deletes only one address from repo
        /// </summary>
        /// <param name="id">Id of address to be deleted</param>
        [TestCase(2)]
        public void RemoveItem_Removes_Only_One_Address(int id)
        {
            // Arrange
            this.addressRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.addressRepoMock.Object.GetAll().Where(a => a.ID_ADDRESS == id).Single());

            // Act
            this.addressLogic.RemoveItem(id);

            // Assert
            Assert.That(this.addressRepoMock.Object.GetAll().Count(), Is.EqualTo(2));
        }

        /// <summary>
        /// RemoveItem() deletes address with given id
        /// </summary>
        /// <param name="id">Id of address to be deleted</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void RemoveItem_Removes_Correct_Address(int id)
        {
            // Arrange
            this.addressRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.addressRepoMock.Object.GetAll().Where(a => a.ID_ADDRESS == id).Single());

            // Act
            this.addressLogic.RemoveItem(id);

            // Assert
            Assert.That(this.addressRepoMock.Object.GetAll().Any(x => x.ID_ADDRESS == id), Is.False);
        }

        /// <summary>
        /// RemoveItem() returns correct string if delete is successfull
        /// </summary>
        /// <param name="id">Id of address to be deleted</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void RemoveItem_Successfull_Remove_Returns_Correct_String(int id)
        {
            // Arrange
            this.addressRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.addressRepoMock.Object.GetAll().Where(a => a.ID_ADDRESS == id).Single());

            // Act, Assert
            Assert.That(this.addressLogic.RemoveItem(id), Is.EqualTo($"ROW WITH ID={id} IS DELETED!"));
        }

        /// <summary>
        /// RemoveItem() returns error message if id not found in db
        /// </summary>
        /// <param name="id">Id of address to be deleted</param>
        [TestCase(-9999)]
        [TestCase(0)]
        [TestCase(9999)]
        public void RemoveItem_If_ID_Not_In_Db_Returns_Correct_String(int id)
        {
            // Arrange
            this.addressRepoMock
                .Setup(repo => repo.Remove(It.IsAny<ADDRESS>()))
                .Throws<ArgumentNullException>();

            // Act / Assert
            Assert.That(this.addressLogic.RemoveItem(id).StartsWith($"ERROR: Table contains no row with ID={id}"), Is.True);
        }

        /// <summary>
        /// RemoveItem() returns error message if exception is thrown
        /// </summary>
        /// <param name="id">Id of address to be deleted</param>
        [TestCase(2)]
        public void RemoveItem_If_Exception_Thrown_Returns_Correct_String(int id)
        {
            // Arrange
            this.addressRepoMock
                .Setup(x => x.Remove(It.IsAny<ADDRESS>()))
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.addressLogic.RemoveItem(id).StartsWith("ERROR: "), Is.True);
        }

        /// <summary>
        /// UpdateItem() succefull update returns correct string
        /// </summary>
        /// <param name="id">id of address to modify</param>
        /// <param name="column">column of address to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 1, "6666")]
        [TestCase(1, 2, "proba2")]
        [TestCase(1, 3, "proba3")]
        [TestCase(1, 4, "proba4")]
        [TestCase(1, 5, "proba5")]
        public void UpdateItem_Succesfull_Update_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.addressRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.addressRepoMock.Object.GetAll().Where(a => a.ID_ADDRESS == id).Single());

            string columnName = " ";
            switch (column)
            {
                case 1:
                    columnName = "ZIP_CODE";
                    break;
                case 2:
                    columnName = "CITY";
                    break;
                case 3:
                    columnName = "STREET_NAME";
                    break;
                case 4:
                    columnName = "STREET_TYPE";
                    break;
                case 5:
                    columnName = "STREET_NUMBER";
                    break;
                default:
                    break;
            }

            // Act / Assert
            Assert.That(this.addressLogic.UpdateItem(id, column, newValue).StartsWith($"ROW WITH ID={id} MODIFIED! {columnName}: NEW VALUE='{newValue}'"), Is.True);
        }

        /// <summary>
        /// UpdateItem() returns error message if id not found in db
        /// </summary>
        /// <param name="id">id of address to modify</param>
        /// <param name="column">column of address to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(4, 1, "6666")]
        public void UpdateItem_If_ID_Not_In_Db_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.addressRepoMock
                .Setup(x => x.Update(It.IsAny<ADDRESS>()))
                .Throws<TargetException>();

            // Act / Assert
            Assert.That(this.addressLogic.UpdateItem(id, column, newValue).Contains($"ERROR: Table contains no row with ID={id}"), Is.True);
        }

        /// <summary>
        /// UpdateItem() returns error message if exception is thrown
        /// </summary>
        /// <param name="id">id of address to modify</param>
        /// <param name="column">column of address to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(2, 1, "6666")]
        public void UpdateItem_If_Exception_Thrown_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.addressRepoMock
                .Setup(x => x.Update(It.IsAny<ADDRESS>()))
                .Throws<Exception>();

            // Act / Assert
            Assert.That(this.addressLogic.UpdateItem(id, column, newValue).StartsWith($"ERROR:"), Is.True);
        }

        /// <summary>
        /// UpdateItem() updates ZIP column
        /// </summary>
        /// <param name="id">id of address to modify</param>
        /// <param name="column">column of address to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 1, "4444")]
        public void UpdateItem_Updates_ZIP_Column(int id, int column, string newValue)
        {
            // Arrange
            this.addressRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.addressRepoMock.Object.GetAll().Where(a => a.ID_ADDRESS == id).Single());

            // Act
            this.addressLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.addressRepoMock.Object.GetItemById(id).ZIP_CODE, Is.EqualTo(4444));
        }

        /// <summary>
        /// UpdateItem() updates CITY column
        /// </summary>
        /// <param name="id">id of address to modify</param>
        /// <param name="column">column of address to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 2, "proba2")]
        public void UpdateItem_Updates_CITY_Column(int id, int column, string newValue)
        {
            // Arrange
            this.addressRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.addressRepoMock.Object.GetAll().Where(a => a.ID_ADDRESS == id).Single());

            // Act
            this.addressLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.addressRepoMock.Object.GetItemById(id).CITY, Is.EqualTo("proba2"));
        }

        /// <summary>
        /// UpdateItem() updates STREET_NAME column
        /// </summary>
        /// <param name="id">id of address to modify</param>
        /// <param name="column">column of address to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 3, "proba3")]
        public void UpdateItem_Updates_STREET_NAME_Column(int id, int column, string newValue)
        {
            // Arrange
            this.addressRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.addressRepoMock.Object.GetAll().Where(a => a.ID_ADDRESS == id).Single());

            // Act
            this.addressLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.addressRepoMock.Object.GetItemById(id).STREET_NAME, Is.EqualTo("proba3"));
        }

        /// <summary>
        /// UpdateItem() updates STREET_TYPE column
        /// </summary>
        /// <param name="id">id of address to modify</param>
        /// <param name="column">column of address to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 4, "proba4")]
        public void UpdateItem_Updates_STREET_TYPE_Column(int id, int column, string newValue)
        {
            // Arrange
            this.addressRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.addressRepoMock.Object.GetAll().Where(a => a.ID_ADDRESS == id).Single());

            // Act
            this.addressLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.addressRepoMock.Object.GetItemById(id).STREET_TYPE, Is.EqualTo("proba4"));
        }

        /// <summary>
        /// UpdateItem() updates STREET_NUMBER column
        /// </summary>
        /// <param name="id">id of address to modify</param>
        /// <param name="column">column of address to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 5, "proba5")]
        public void UpdateItem_Updates_STREET_NUMBER_Column(int id, int column, string newValue)
        {
            // Arrange
            this.addressRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.addressRepoMock.Object.GetAll().Where(a => a.ID_ADDRESS == id).Single());

            // Act
            this.addressLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.addressRepoMock.Object.GetItemById(id).STREET_NUMBER, Is.EqualTo("proba5"));
        }

        /// <summary>
        /// ListItems() if exception is thrown returns correct string
        /// </summary>
        [Test]
        public void ListItems_If_Exception_Thrown_Returns_Correct_String()
        {
            // Arrange
            this.addressRepoMock
                .Setup(x => x.GetAll())
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.addressLogic.ListItems().StartsWith("ERROR:"), Is.True);
        }

        /// <summary>
        /// ListItems() returns string contains DB data
        /// </summary>
        [Test]
        public void ListItems_Returns_String_Contains_Db_Data()
        {
            // Arrange

            // Act, Assert
            Assert.That(this.addressLogic.ListItems().Contains("ID_ADDRESS"),    Is.True);
            Assert.That(this.addressLogic.ListItems().Contains("ZIP_CODE"),      Is.True);
            Assert.That(this.addressLogic.ListItems().Contains("CITY"),          Is.True);
            Assert.That(this.addressLogic.ListItems().Contains("STREET_NAME"),   Is.True);
            Assert.That(this.addressLogic.ListItems().Contains("STREET_TYPE"),   Is.True);
            Assert.That(this.addressLogic.ListItems().Contains("STREET_NUMBER"), Is.True);
            Assert.That(this.addressLogic.ListItems().Contains("város1"),        Is.True);
            Assert.That(this.addressLogic.ListItems().Contains("utca2"),         Is.True);
            Assert.That(this.addressLogic.ListItems().Contains("2000"),          Is.True);
            Assert.That(this.addressLogic.ListItems().Contains("utcatípus3"),    Is.True);
            Assert.That(this.addressLogic.ListItems().Contains("házszám3"),      Is.True);
            Assert.That(this.addressLogic.ListItems().Contains("1000"),          Is.True);
        }
    }
}
