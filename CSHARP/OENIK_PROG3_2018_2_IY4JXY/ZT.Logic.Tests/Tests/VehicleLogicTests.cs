﻿// <copyright file="VehicleLogicTests.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Tests.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Moq;
    using NUnit.Framework;
    using ZT.Data;
    using ZT.Logic.Logics;
    using ZT.Repository;

    /// <summary>
    /// Vehicles table CRUD logic nunit tests
    /// </summary>
    [TestFixture]
    public class VehicleLogicTests
    {
        private Mock<IRepository<VEHICLE>> vehiclesRepoMock;
        private VehiclesLogic vehiclesLogic;

        /// <summary>
        /// Gets three testCaseData for drivers logic methods
        /// </summary>
        public static IEnumerable<TestCaseData> ThreeVehiclesTestCaseData
        {
            get
            {
                List<TestCaseData> testCases = new List<TestCaseData>()
                {
                    new TestCaseData((object)new string[] { "márka4", "modell4", "szín4", "2010", "40000" }),
                    new TestCaseData((object)new string[] { "márka5", "modell5", "szín5", "2020", "50000" }),
                    new TestCaseData((object)new string[] { "márka6", "modell6", "szín6", "2030", "60000" })
                };
                return testCases;
            }
        }

        /// <summary>
        /// Setup mock for passengers logic testing
        /// </summary>
        [SetUp]
        public void SetupVehiclesRepo()
        {
            this.vehiclesRepoMock = new Mock<IRepository<VEHICLE>>();
            List<VEHICLE> vehiclesList = new List<VEHICLE>()
            {
                new VEHICLE() { ID_VEHICLE = 1, BRAND = "márka1", MODELL = "modell1", COLOR = "szín1", AGE_GROUP = 1980, SPEEDOMETER_STATE = 100000 },
                new VEHICLE() { ID_VEHICLE = 2, BRAND = "márka2", MODELL = "modell2", COLOR = "szín2", AGE_GROUP = 1990, SPEEDOMETER_STATE = 200000 },
                new VEHICLE() { ID_VEHICLE = 3, BRAND = "márka3", MODELL = "modell3", COLOR = "szín3", AGE_GROUP = 2000, SPEEDOMETER_STATE = 300000 }
            };

            this.vehiclesRepoMock
                .Setup(repo => repo.GetAll())
                .Returns(vehiclesList.AsQueryable());

            this.vehiclesRepoMock
                .Setup(repo => repo.Insert(It.IsAny<VEHICLE>()))
                .Callback<VEHICLE>(v => vehiclesList.Add(v));

            this.vehiclesRepoMock
                .Setup(repo => repo.Remove(It.IsAny<VEHICLE>()))
                .Callback<VEHICLE>(v => vehiclesList.Remove(v));

            this.vehiclesLogic = new VehiclesLogic(this.vehiclesRepoMock.Object);
        }

        /// <summary>
        /// AddNewItem() method adds only one item
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "márka7", "modell7", "szín7", "2070", "70000" })]
        public void AddNewItem_Adds_Only_One_Item_To_Repository(string[] newData)
        {
            // Arrange

            // Act
            this.vehiclesLogic.AddNewItem(newData);

            // Assert
            Assert.That(this.vehiclesRepoMock.Object.GetAll().Count(), Is.EqualTo(4));
        }

        /// <summary>
        /// AddNewItem() successfull insert returns correct string
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "márka7", "modell7", "szín7", "2070", "70000" })]
        public void AddNewItem_Successfull_Insert_Returns_Correct_String_(string[] newData)
        {
            // Arrange

            // Act, Assert
            Assert.That(this.vehiclesLogic.AddNewItem(newData), Is.EqualTo("NEW ITEM ADDED!"));
        }

        /// <summary>
        /// AddNewItem() method adds correct item to repository
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "márka7", "modell7", "szín7", "2070", "70000" })]
        public void AddNewItem_If_Exception_Thrown_Returns_Correct_String(string[] newData)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(x => x.Insert(It.IsAny<VEHICLE>()))
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.vehiclesLogic.AddNewItem(newData).StartsWith("ERROR: "), Is.True);
        }

        /// <summary>
        /// AddNewItem() method adds correct item to repository
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCaseSource("ThreeVehiclesTestCaseData")]
        public void AddNewItem_Adds_Correct_Item_To_DriversRepo(string[] newData)
        {
            // Arrange

            // Act
            this.vehiclesLogic.AddNewItem(newData);

            // Assert
            Assert.That(this.vehiclesRepoMock.Object.GetAll().Last().BRAND,             Is.EqualTo(newData[0]));
            Assert.That(this.vehiclesRepoMock.Object.GetAll().Last().MODELL,            Is.EqualTo(newData[1]));
            Assert.That(this.vehiclesRepoMock.Object.GetAll().Last().COLOR,             Is.EqualTo(newData[2]));
            Assert.That(this.vehiclesRepoMock.Object.GetAll().Last().AGE_GROUP,         Is.EqualTo(int.Parse(newData[3])));
            Assert.That(this.vehiclesRepoMock.Object.GetAll().Last().SPEEDOMETER_STATE, Is.EqualTo(int.Parse(newData[4])));
        }

        /// <summary>
        /// RemoveItem() deletes only one item from repo
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(2)]
        public void RemoveItem_Removes_Only_One_Item(int id)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.vehiclesRepoMock.Object.GetAll().Where(a => a.ID_VEHICLE == id).Single());

            // Act
            this.vehiclesLogic.RemoveItem(id);

            // Assert
            Assert.That(this.vehiclesRepoMock.Object.GetAll().Count(), Is.EqualTo(2));
        }

        /// <summary>
        /// RemoveItem() deletes item with given id
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void RemoveItem_Removes_Correct_Item(int id)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.vehiclesRepoMock.Object.GetAll().Where(a => a.ID_VEHICLE == id).Single());

            // Act
            this.vehiclesLogic.RemoveItem(id);

            // Assert
            Assert.That(this.vehiclesRepoMock.Object.GetAll().Any(x => x.ID_VEHICLE == id), Is.False);
        }

        /// <summary>
        /// RemoveItem() returns correct string if delete is successfull
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void RemoveItem_Successfull_Remove_Returns_Correct_String(int id)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.vehiclesRepoMock.Object.GetAll().Where(a => a.ID_VEHICLE == id).Single());

            // Act, Assert
            Assert.That(this.vehiclesLogic.RemoveItem(id), Is.EqualTo($"ROW WITH ID={id} IS DELETED!"));
        }

        /// <summary>
        /// RemoveItem() returns error message if id not found in db
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(0)]
        [TestCase(9999)]
        public void RemoveItem_If_ID_Not_In_Db_Returns_Correct_String(int id)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(repo => repo.Remove(It.IsAny<VEHICLE>()))
                .Throws<ArgumentNullException>();

            // Act / Assert
            Assert.That(this.vehiclesLogic.RemoveItem(id).StartsWith($"ERROR: Table contains no row with ID={id}"), Is.True);
        }

        /// <summary>
        /// RemoveItem() returns error message if exception is thrown
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(2)]
        public void RemoveItem_If_Exception_Thrown_Returns_Correct_String(int id)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(x => x.Remove(It.IsAny<VEHICLE>()))
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.vehiclesLogic.RemoveItem(id).StartsWith("ERROR: "), Is.True);
        }

        /// <summary>
        /// UpdateItem() succefull update returns correct string
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 1, "proba1")]
        [TestCase(1, 2, "proba2")]
        [TestCase(1, 3, "proba3")]
        [TestCase(1, 4, "2000")]
        [TestCase(1, 5, "10000")]
        public void UpdateItem_Succesfull_Update_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.vehiclesRepoMock.Object.GetAll().Where(a => a.ID_VEHICLE == id).Single());

            string columnName = " ";
            switch (column)
            {
                case 1:
                    columnName = "BRAND";
                    break;
                case 2:
                    columnName = "MODELL";
                    break;
                case 3:
                    columnName = "COLOR";
                    break;
                case 4:
                    columnName = "AGE_GROUP";
                    break;
                case 5:
                    columnName = "SPEEDOMETER_STATE";
                    break;
                default:
                    break;
            }

            // Act / Assert
            Assert.That(this.vehiclesLogic.UpdateItem(id, column, newValue).StartsWith($"ROW WITH ID={id} MODIFIED! {columnName}: NEW VALUE='{newValue}'"), Is.True);
        }

        /// <summary>
        /// UpdateItem() returns error message if id not found in db
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(4, 1, "proba1")]
        public void UpdateItem_If_ID_Not_In_Db_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(x => x.Update(It.IsAny<VEHICLE>()))
                .Throws<TargetException>();

            // Act / Assert
            Assert.That(this.vehiclesLogic.UpdateItem(id, column, newValue).Contains($"ERROR: Table contains no row with ID={id}"), Is.True);
        }

        /// <summary>
        /// UpdateItem() returns error message if exception is thrown
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(2, 1, "proba1")]
        public void UpdateItem_If_Exception_Thrown_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(x => x.Update(It.IsAny<VEHICLE>()))
                .Throws<Exception>();

            // Act / Assert
            Assert.That(this.vehiclesLogic.UpdateItem(id, column, newValue).StartsWith($"ERROR:"), Is.True);
        }

        /// <summary>
        /// UpdateItem() updates BRAND column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 1, "proba1")]
        public void UpdateItem_Updates_BRAND_Column(int id, int column, string newValue)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.vehiclesRepoMock.Object.GetAll().Where(a => a.ID_VEHICLE == id).Single());

            // Act
            this.vehiclesLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.vehiclesRepoMock.Object.GetItemById(id).BRAND, Is.EqualTo("proba1"));
        }

        /// <summary>
        /// UpdateItem() updates MODEL column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 2, "proba2")]
        public void UpdateItem_Updates_MODEL_Column(int id, int column, string newValue)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.vehiclesRepoMock.Object.GetAll().Where(a => a.ID_VEHICLE == id).Single());

            // Act
            this.vehiclesLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.vehiclesRepoMock.Object.GetItemById(id).MODELL, Is.EqualTo("proba2"));
        }

        /// <summary>
        /// UpdateItem() updates COLOR column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 3, "proba2")]
        public void UpdateItem_Updates_COLOR_Column(int id, int column, string newValue)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.vehiclesRepoMock.Object.GetAll().Where(a => a.ID_VEHICLE == id).Single());

            // Act
            this.vehiclesLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.vehiclesRepoMock.Object.GetItemById(id).COLOR, Is.EqualTo("proba2"));
        }

        /// <summary>
        /// UpdateItem() updates AGE_GROUP column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 4, "2000")]
        public void UpdateItem_Updates_AGE_GROUP_Column(int id, int column, string newValue)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.vehiclesRepoMock.Object.GetAll().Where(a => a.ID_VEHICLE == id).Single());

            // Act
            this.vehiclesLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.vehiclesRepoMock.Object.GetItemById(id).AGE_GROUP, Is.EqualTo(2000));
        }

        /// <summary>
        /// UpdateItem() updates AGE_SPEEDOMETER_STATE column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 5, "20000")]
        public void UpdateItem_Updates_SPEEDOMETER_STATE_Column(int id, int column, string newValue)
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.vehiclesRepoMock.Object.GetAll().Where(a => a.ID_VEHICLE == id).Single());

            // Act
            this.vehiclesLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.vehiclesRepoMock.Object.GetItemById(id).SPEEDOMETER_STATE, Is.EqualTo(20000));
        }

        /// <summary>
        /// ListItems() if exception is thrown returns correct string
        /// </summary>
        [Test]
        public void ListItems_If_Exception_Thrown_Returns_Correct_String()
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(x => x.GetAll())
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.vehiclesLogic.ListItems().StartsWith("ERROR:"), Is.True);
        }

        /// <summary>
        /// ListItems() return string contains DB data
        /// </summary>
        [Test]
        public void ListItems_Return_String_Contains_Db_Data()
        {
            // Arrange

            // Act, Assert
            Assert.That(this.vehiclesLogic.ListItems().Contains("BRAND"), Is.True);
            Assert.That(this.vehiclesLogic.ListItems().Contains("MODELL"), Is.True);
            Assert.That(this.vehiclesLogic.ListItems().Contains("COLOR"), Is.True);
            Assert.That(this.vehiclesLogic.ListItems().Contains("AGE_GROUP"), Is.True);
            Assert.That(this.vehiclesLogic.ListItems().Contains("SPEEDOMETER_STATE"), Is.True);
            Assert.That(this.vehiclesLogic.ListItems().Contains("márka1"), Is.True);
            Assert.That(this.vehiclesLogic.ListItems().Contains("modell2"), Is.True);
            Assert.That(this.vehiclesLogic.ListItems().Contains("szín3"), Is.True);
            Assert.That(this.vehiclesLogic.ListItems().Contains("1990"), Is.True);
            Assert.That(this.vehiclesLogic.ListItems().Contains("30000"), Is.True);
        }
    }
}
