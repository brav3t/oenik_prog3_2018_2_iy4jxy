﻿// <copyright file="DriversLogicTests.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Tests.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Moq;
    using NUnit.Framework;
    using ZT.Data;
    using ZT.Logic.Logics;
    using ZT.Repository;

    /// <summary>
    /// Drivers table CRUD logic unit tests
    /// </summary>
    [TestFixture]
    public class DriversLogicTests
    {
        private Mock<IRepository<DRIVER>> driversRepoMock;
        private DriversLogic driversLogic;

        /// <summary>
        /// Gets three testCaseData for drivers logic methods
        /// </summary>
        public static IEnumerable<TestCaseData> ThreeDriversTestCaseData
        {
            get
            {
                List<TestCaseData> testCases = new List<TestCaseData>()
                {
                    new TestCaseData((object)new string[] { "vezeték4", "kereszt4", "2000.10.10", "6", "0" }),
                    new TestCaseData((object)new string[] { "vezeték5", "kereszt5", "2000.11.11", "7", "0" }),
                    new TestCaseData((object)new string[] { "vezeték6", "kereszt6", "2000.12.12", "8", "0" }),
                };
                return testCases;
            }
        }

        /// <summary>
        /// Setup mock for drivers logic testing
        /// </summary>
        [SetUp]
        public void SetupDriversRepo()
        {
            this.driversRepoMock = new Mock<IRepository<DRIVER>>();
            List<DRIVER> driversList = new List<DRIVER>()
            {
                new DRIVER() { ID_DRIVER = 1, LAST_NAME = "vezeték1", FIRST_NAME = "kereszt1", STARTDATE_OF_WORK = new DateTime(2000, 07, 07), EXPERIENCE = 3, POINTS = 0 },
                new DRIVER() { ID_DRIVER = 2, LAST_NAME = "vezeték2", FIRST_NAME = "kereszt2", STARTDATE_OF_WORK = new DateTime(2000, 08, 08), EXPERIENCE = 4, POINTS = 0 },
                new DRIVER() { ID_DRIVER = 3, LAST_NAME = "vezeték3", FIRST_NAME = "kereszt3", STARTDATE_OF_WORK = new DateTime(2000, 09, 09), EXPERIENCE = 5, POINTS = 0 }
            };

            this.driversRepoMock
                .Setup(repo => repo.GetAll())
                .Returns(driversList.AsQueryable());

            this.driversRepoMock
                .Setup(repo => repo.Insert(It.IsAny<DRIVER>()))
                .Callback<DRIVER>(d => driversList.Add(d));

            this.driversRepoMock
                .Setup(repo => repo.Remove(It.IsAny<DRIVER>()))
                .Callback<DRIVER>(d => driversList.Remove(d));

            this.driversLogic = new DriversLogic(this.driversRepoMock.Object);
        }

        /// <summary>
        /// AddNewItem() method adds only one item
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "vezeték7", "kereszt7", "2000.06.06", "6", "0" })]
        public void AddNewItem_Adds_Only_One_Driver_To_Repository(string[] newData)
        {
            // Arrange

            // Act
            this.driversLogic.AddNewItem(newData);

            // Assert
            Assert.That(this.driversRepoMock.Object.GetAll().Count(), Is.EqualTo(4));
        }

        /// <summary>
        /// AddNewItem() successfull insert returns correct string
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "vezeték7", "kereszt7", "2000.06.06", "6", "0" })]
        public void AddNewItem_Successfull_Insert_Returns_Correct_String_(string[] newData)
        {
            // Arrange

            // Act, Assert
            Assert.That(this.driversLogic.AddNewItem(newData), Is.EqualTo("NEW ITEM ADDED!"));
        }

        /// <summary>
        /// AddNewItem() method adds correct item to repository
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "vezeték7", "kereszt7", "2000.06.06", "6", "0" })]
        public void AddNewItem_If_Exception_Thrown_Returns_Correct_String(string[] newData)
        {
            // Arrange
            this.driversRepoMock
                .Setup(x => x.Insert(It.IsAny<DRIVER>()))
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.driversLogic.AddNewItem(newData).StartsWith("ERROR: "), Is.True);
        }

        /// <summary>
        /// AddNewItem() method adds correct item to repository
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCaseSource("ThreeDriversTestCaseData")]
        public void AddNewItem_Adds_Correct_Driver_To_DriversRepo(string[] newData)
        {
            // Arrange

            // Act
            this.driversLogic.AddNewItem(newData);

            // Assert
            Assert.That(this.driversRepoMock.Object.GetAll().Last().LAST_NAME,         Is.EqualTo(newData[0]));
            Assert.That(this.driversRepoMock.Object.GetAll().Last().FIRST_NAME,        Is.EqualTo(newData[1]));
            Assert.That(this.driversRepoMock.Object.GetAll().Last().STARTDATE_OF_WORK, Is.EqualTo(Convert.ToDateTime(newData[2])));
            Assert.That(this.driversRepoMock.Object.GetAll().Last().EXPERIENCE,        Is.EqualTo(int.Parse(newData[3])));
            Assert.That(this.driversRepoMock.Object.GetAll().Last().POINTS,            Is.EqualTo(int.Parse(newData[4])));
        }

        /// <summary>
        /// RemoveItem() deletes only one item from repo
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(2)]
        public void RemoveItem_Removes_Only_One_Driver(int id)
        {
            // Arrange
            this.driversRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.driversRepoMock.Object.GetAll().Where(a => a.ID_DRIVER == id).Single());

            // Act
            this.driversLogic.RemoveItem(id);

            // Assert
            Assert.That(this.driversRepoMock.Object.GetAll().Count(), Is.EqualTo(2));
        }

        /// <summary>
        /// RemoveItem() deletes item with given id
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void RemoveItem_Removes_Correct_Driver(int id)
        {
            // Arrange
            this.driversRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.driversRepoMock.Object.GetAll().Where(a => a.ID_DRIVER == id).Single());

            // Act
            this.driversLogic.RemoveItem(id);

            // Assert
            Assert.That(this.driversRepoMock.Object.GetAll().Any(x => x.ID_DRIVER == id), Is.False);
        }

        /// <summary>
        /// RemoveItem() returns correct string if delete is successfull
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void RemoveItem_Successfull_Remove_Returns_Correct_String(int id)
        {
            // Arrange
            this.driversRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.driversRepoMock.Object.GetAll().Where(a => a.ID_DRIVER == id).Single());

            // Act, Assert
            Assert.That(this.driversLogic.RemoveItem(id), Is.EqualTo($"ROW WITH ID={id} IS DELETED!"));
        }

        /// <summary>
        /// RemoveItem() returns error message if id not found in db
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(-9999)]
        [TestCase(0)]
        [TestCase(9999)]
        public void RemoveItem_If_ID_Not_In_Db_Returns_Correct_String(int id)
        {
            // Arrange
            this.driversRepoMock
                .Setup(repo => repo.Remove(It.IsAny<DRIVER>()))
                .Throws<ArgumentNullException>();

            // Act / Assert
            Assert.That(this.driversLogic.RemoveItem(id).StartsWith($"ERROR: Table contains no row with ID={id}"), Is.True);
        }

        /// <summary>
        /// RemoveItem() returns error message if exception is thrown
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(2)]
        public void RemoveItem_If_Exception_Thrown_Returns_Correct_String(int id)
        {
            // Arrange
            this.driversRepoMock
                .Setup(x => x.Remove(It.IsAny<DRIVER>()))
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.driversLogic.RemoveItem(id).StartsWith("ERROR: "), Is.True);
        }

        /// <summary>
        /// UpdateItem() succefull update returns correct string
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 1, "proba1")]
        [TestCase(1, 2, "proba2")]
        [TestCase(1, 3, "2000.10.10")]
        [TestCase(1, 4, "5")]
        [TestCase(1, 5, "0")]
        public void UpdateItem_Succesfull_Update_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.driversRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.driversRepoMock.Object.GetAll().Where(a => a.ID_DRIVER == id).Single());

            string columnName = " ";
            switch (column)
            {
                case 1:
                    columnName = "LAST_NAME";
                    break;
                case 2:
                    columnName = "FIRST_NAME";
                    break;
                case 3:
                    columnName = "STARTDATE_OF_WORK";
                    break;
                case 4:
                    columnName = "EXPERIENCE";
                    break;
                case 5:
                    columnName = "POINTS";
                    break;
                default:
                    break;
            }

            // Act / Assert
            Assert.That(this.driversLogic.UpdateItem(id, column, newValue).StartsWith($"ROW WITH ID={id} MODIFIED! {columnName}: NEW VALUE='{newValue}'"), Is.True);
        }

        /// <summary>
        /// UpdateItem() returns error message if id not found in db
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(4, 1, "6666")]
        public void UpdateItem_If_ID_Not_In_Db_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.driversRepoMock
                .Setup(x => x.Update(It.IsAny<DRIVER>()))
                .Throws<TargetException>();

            // Act / Assert
            Assert.That(this.driversLogic.UpdateItem(id, column, newValue).Contains($"ERROR: Table contains no row with ID={id}"), Is.True);
        }

        /// <summary>
        /// UpdateItem() returns error message if exception is thrown
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(2, 1, "6666")]
        public void UpdateItem_If_Exception_Thrown_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.driversRepoMock
                .Setup(x => x.Update(It.IsAny<DRIVER>()))
                .Throws<Exception>();

            // Act / Assert
            Assert.That(this.driversLogic.UpdateItem(id, column, newValue).StartsWith($"ERROR:"), Is.True);
        }

        /// <summary>
        /// UpdateItem() updates LAST_NAME column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 1, "proba1")]
        public void UpdateItem_Updates_LAST_NAME_Column(int id, int column, string newValue)
        {
            // Arrange
            this.driversRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.driversRepoMock.Object.GetAll().Where(a => a.ID_DRIVER == id).Single());

            // Act
            this.driversLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.driversRepoMock.Object.GetItemById(id).LAST_NAME, Is.EqualTo("proba1"));
        }

        /// <summary>
        /// UpdateItem() updates FIRST column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 2, "proba2")]
        public void UpdateItem_Updates_FIRST_NAME_Column(int id, int column, string newValue)
        {
            // Arrange
            this.driversRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.driversRepoMock.Object.GetAll().Where(a => a.ID_DRIVER == id).Single());

            // Act
            this.driversLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.driversRepoMock.Object.GetItemById(id).FIRST_NAME, Is.EqualTo("proba2"));
        }

        /// <summary>
        /// UpdateItem() updates STARTDATE_OF_WORK column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 3, "2000.10.10")]
        public void UpdateItem_Updates_STARTDATE_OF_WORK_Column(int id, int column, string newValue)
        {
            // Arrange
            this.driversRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.driversRepoMock.Object.GetAll().Where(a => a.ID_DRIVER == id).Single());

            // Act
            this.driversLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.driversRepoMock.Object.GetItemById(id).STARTDATE_OF_WORK.ToString().Contains("2000"), Is.True);
        }

        /// <summary>
        /// UpdateItem() updates EXPERIENCE column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 4, "50")]
        public void UpdateItem_Updates_EXPERIENCE_Column(int id, int column, string newValue)
        {
            // Arrange
            this.driversRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.driversRepoMock.Object.GetAll().Where(a => a.ID_DRIVER == id).Single());

            // Act
            this.driversLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.driversRepoMock.Object.GetItemById(id).EXPERIENCE, Is.EqualTo(50));
        }

        /// <summary>
        /// UpdateItem() updates POINTS column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 5, "100")]
        public void UpdateItem_Updates_POINTS_Column(int id, int column, string newValue)
        {
            // Arrange
            this.driversRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.driversRepoMock.Object.GetAll().Where(a => a.ID_DRIVER == id).Single());

            // Act
            this.driversLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.driversRepoMock.Object.GetItemById(id).POINTS, Is.EqualTo(100));
        }

        /// <summary>
        /// ListItems() if exception is thrown returns correct string
        /// </summary>
        [Test]
        public void ListItems_If_Exception_Thrown_Returns_Correct_String()
        {
            // Arrange
            this.driversRepoMock
                .Setup(x => x.GetAll())
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.driversLogic.ListItems().StartsWith("ERROR:"), Is.True);
        }

        /// <summary>
        /// ListItems() return string contains DB data
        /// </summary>
        [Test]
        public void ListItems_Return_String_Contains_Db_Data()
        {
            // Arrange

            // Act, Assert
            // Assert.That(this.driversLogic.ListItems().Contains("2000.08.08 - 00:00"), Is.True);
            Assert.That(this.driversLogic.ListItems().Contains("ID_DRIVER"),          Is.True);
            Assert.That(this.driversLogic.ListItems().Contains("LAST_NAME"),          Is.True);
            Assert.That(this.driversLogic.ListItems().Contains("FIRST_NAME"),         Is.True);
            Assert.That(this.driversLogic.ListItems().Contains("STARTDATE_OF_WORK"),  Is.True);
            Assert.That(this.driversLogic.ListItems().Contains("EXPERIENCE"),         Is.True);
            Assert.That(this.driversLogic.ListItems().Contains("POINTS"),             Is.True);
            Assert.That(this.driversLogic.ListItems().Contains("vezeték1"),           Is.True);
            Assert.That(this.driversLogic.ListItems().Contains("kereszt2"),           Is.True);
            Assert.That(this.driversLogic.ListItems().Contains("5"),                  Is.True);
            Assert.That(this.driversLogic.ListItems().Contains("0"),                  Is.True);
            Assert.That(this.driversLogic.ListItems().Contains("kereszt3"),           Is.True);
        }
    }
}
