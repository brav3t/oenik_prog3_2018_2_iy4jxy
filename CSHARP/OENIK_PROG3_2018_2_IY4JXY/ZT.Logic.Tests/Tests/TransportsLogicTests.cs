﻿// <copyright file="TransportsLogicTests.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Tests.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Moq;
    using NUnit.Framework;
    using ZT.Data;
    using ZT.Logic.Logics;
    using ZT.Repository;

    /// <summary>
    /// Transports table CRUD logic nunit tests
    /// </summary>
    [TestFixture]
    public class TransportsLogicTests
    {
        private Mock<IRepository<TRANSPORT>> transportsRepoMock;
        private TransportsLogic transportsLogic;

        /// <summary>
        /// Gets three testCaseData for drivers logic methods
        /// </summary>
        public static IEnumerable<TestCaseData> ThreeTransportsTestCaseData
        {
            get
            {
                List<TestCaseData> testCases = new List<TestCaseData>()
                {
                    new TestCaseData((object)new string[] { "4", "4", "2004-04-04 04:00", "4", "2004-04-04 04:04", "5", "4" }),
                    new TestCaseData((object)new string[] { "5", "5", "2005-05-05 05:00", "5", "2005-05-05 05:05", "6", "5" }),
                    new TestCaseData((object)new string[] { "6", "6", "2006-06-06 06:00", "6", "2006-06-06 06:06", "7", "6" }),
                };
                return testCases;
            }
        }

        /// <summary>
        /// Setup mock for passengers logic testing
        /// </summary>
        [SetUp]
        public void SetupVehiclesRepo()
        {
            this.transportsRepoMock = new Mock<IRepository<TRANSPORT>>();
            List<TRANSPORT> transportsList = new List<TRANSPORT>()
            {
                new TRANSPORT() { ID_TRANSPORT = 1, DRIVER_ID = 1, PASSENGER_ID = 1, DEPARTURE_TIME = new DateTime(2001, 01, 01, 01, 00, 00), DEPARTURE_ADDR_ID = 1, ARRIVE_TIME = new DateTime(2001, 01, 01, 01, 01, 00), DESTINATION_ADDR_ID = 2, VEHICLE_ID = 1 },
                new TRANSPORT() { ID_TRANSPORT = 2, DRIVER_ID = 2, PASSENGER_ID = 2, DEPARTURE_TIME = new DateTime(2002, 02, 02, 02, 00, 00), DEPARTURE_ADDR_ID = 2, ARRIVE_TIME = new DateTime(2002, 02, 02, 02, 02, 00), DESTINATION_ADDR_ID = 3, VEHICLE_ID = 2 },
                new TRANSPORT() { ID_TRANSPORT = 3, DRIVER_ID = 3, PASSENGER_ID = 3, DEPARTURE_TIME = new DateTime(2003, 03, 03, 03, 00, 00), DEPARTURE_ADDR_ID = 3, ARRIVE_TIME = new DateTime(2003, 03, 03, 03, 03, 00), DESTINATION_ADDR_ID = 4, VEHICLE_ID = 3 },
            };

            this.transportsRepoMock
                .Setup(repo => repo.GetAll())
                .Returns(transportsList.AsQueryable());

            this.transportsRepoMock
                .Setup(repo => repo.Insert(It.IsAny<TRANSPORT>()))
                .Callback<TRANSPORT>(v => transportsList.Add(v));

            this.transportsRepoMock
                .Setup(repo => repo.Remove(It.IsAny<TRANSPORT>()))
                .Callback<TRANSPORT>(v => transportsList.Remove(v));

            this.transportsLogic = new TransportsLogic(this.transportsRepoMock.Object);
        }

        /// <summary>
        /// AddNewItem() method adds only one item
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "7", "7", "2007-07-07 07:00", "7", "2007-07-07 07:07", "8", "7" })]
        public void AddNewItem_Adds_Only_One_Item_To_Repository(string[] newData)
        {
            // Arrange

            // Act
            this.transportsLogic.AddNewItem(newData);

            // Assert
            Assert.That(this.transportsRepoMock.Object.GetAll().Count(), Is.EqualTo(4));
        }

        /// <summary>
        /// AddNewItem() successfull insert returns correct string
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "7", "7", "2007-07-07 07:00", "7", "2007-07-07 07:07", "8", "7" })]
        public void AddNewItem_Successfull_Insert_Returns_Correct_String_(string[] newData)
        {
            // Arrange

            // Act, Assert
            Assert.That(this.transportsLogic.AddNewItem(newData), Is.EqualTo("NEW ITEM ADDED!"));
        }

        /// <summary>
        /// AddNewItem() method adds correct item to repository
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "7", "7", "2007-07-07 07:00", "7", "2007-07-07 07:07", "8", "7" })]
        public void AddNewItem_If_Exception_Thrown_Returns_Correct_String(string[] newData)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(x => x.Insert(It.IsAny<TRANSPORT>()))
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.transportsLogic.AddNewItem(newData).StartsWith("ERROR: "), Is.True);
        }

        /// <summary>
        /// AddNewItem() method adds correct item to repository
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCaseSource("ThreeTransportsTestCaseData")]
        public void AddNewItem_Adds_Correct_Item_To_DriversRepo(string[] newData)
        {
            // Arrange

            // Act
            this.transportsLogic.AddNewItem(newData);

            // Assert
            // Assert.That(this.transportsRepoMock.Object.GetAll().Last().DEPARTURE_TIME.ToString(), Is.EqualTo(newData[2]));
            // Assert.That(this.transportsRepoMock.Object.GetAll().Last().ARRIVE_TIME.ToString(), Is.EqualTo(newData[4]));
            Assert.That(this.transportsRepoMock.Object.GetAll().Last().DRIVER_ID, Is.EqualTo(int.Parse(newData[0])));
            Assert.That(this.transportsRepoMock.Object.GetAll().Last().PASSENGER_ID, Is.EqualTo(int.Parse(newData[1])));
            Assert.That(this.transportsRepoMock.Object.GetAll().Last().DEPARTURE_ADDR_ID, Is.EqualTo(int.Parse(newData[3])));
            Assert.That(this.transportsRepoMock.Object.GetAll().Last().DESTINATION_ADDR_ID, Is.EqualTo(int.Parse(newData[5])));
            Assert.That(this.transportsRepoMock.Object.GetAll().Last().VEHICLE_ID, Is.EqualTo(int.Parse(newData[6])));
        }

        /// <summary>
        /// RemoveItem() deletes only one item from repo
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(2)]
        public void RemoveItem_Removes_Only_One_Item(int id)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.transportsRepoMock.Object.GetAll().Where(a => a.ID_TRANSPORT == id).Single());

            // Act
            this.transportsLogic.RemoveItem(id);

            // Assert
            Assert.That(this.transportsRepoMock.Object.GetAll().Count(), Is.EqualTo(2));
        }

        /// <summary>
        /// RemoveItem() deletes item with given id
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void RemoveItem_Removes_Correct_Item(int id)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.transportsRepoMock.Object.GetAll().Where(a => a.ID_TRANSPORT == id).Single());

            // Act
            this.transportsLogic.RemoveItem(id);

            // Assert
            Assert.That(this.transportsRepoMock.Object.GetAll().Any(x => x.ID_TRANSPORT == id), Is.False);
        }

        /// <summary>
        /// RemoveItem() returns correct string if delete is successfull
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void RemoveItem_Successfull_Remove_Returns_Correct_String(int id)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.transportsRepoMock.Object.GetAll().Where(a => a.ID_TRANSPORT == id).Single());

            // Act, Assert
            Assert.That(this.transportsLogic.RemoveItem(id), Is.EqualTo($"ROW WITH ID={id} IS DELETED!"));
        }

        /// <summary>
        /// RemoveItem() returns error message if id not found in db
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(0)]
        [TestCase(9999)]
        public void RemoveItem_If_ID_Not_In_Db_Returns_Correct_String(int id)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(repo => repo.Remove(It.IsAny<TRANSPORT>()))
                .Throws<ArgumentNullException>();

            // Act / Assert
            Assert.That(this.transportsLogic.RemoveItem(id).StartsWith($"ERROR: Table contains no row with ID={id}"), Is.True);
        }

        /// <summary>
        /// RemoveItem() returns error message if exception is thrown
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(2)]
        public void RemoveItem_If_Exception_Thrown_Returns_Correct_String(int id)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(x => x.Remove(It.IsAny<TRANSPORT>()))
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.transportsLogic.RemoveItem(id).StartsWith("ERROR: "), Is.True);
        }

        /// <summary>
        /// UpdateItem() succefull update returns correct string
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 3, "2007-07-07 07:00")]
        [TestCase(1, 5, "2007-07-07 07:00")]
        [TestCase(1, 1, "10")]
        [TestCase(1, 2, "10")]
        [TestCase(1, 4, "10")]
        [TestCase(1, 6, "10")]
        [TestCase(1, 7, "10")]
        public void UpdateItem_Succesfull_Update_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.transportsRepoMock.Object.GetAll().Where(a => a.ID_TRANSPORT == id).Single());

            string columnName = " ";
            switch (column)
            {
                case 1:
                    columnName = "DRIVER_ID";
                    break;
                case 2:
                    columnName = "PASSENGER_ID";
                    break;
                case 3:
                    columnName = "DEPARTURE_TIME";
                    break;
                case 4:
                    columnName = "DEPARTURE_ADDR_ID";
                    break;
                case 5:
                    columnName = "ARRIVE_TIME";
                    break;
                case 6:
                    columnName = "DESTINATION_ADDR_ID";
                    break;
                case 7:
                    columnName = "VEHICLE_ID";
                    break;
                default:
                    break;
            }

            // Act / Assert
            Assert.That(this.transportsLogic.UpdateItem(id, column, newValue).StartsWith($"ROW WITH ID={id} MODIFIED! {columnName}: NEW VALUE='{newValue}'"), Is.True);
        }

        /// <summary>
        /// UpdateItem() returns error message if id not found in db
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(4, 1, "proba1")]
        public void UpdateItem_If_ID_Not_In_Db_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(x => x.Update(It.IsAny<TRANSPORT>()))
                .Throws<TargetException>();

            // Act / Assert
            Assert.That(this.transportsLogic.UpdateItem(id, column, newValue).Contains($"ERROR: Table contains no row with ID={id}"), Is.True);
        }

        /// <summary>
        /// UpdateItem() returns error message if exception is thrown
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(2, 1, "proba1")]
        public void UpdateItem_If_Exception_Thrown_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(x => x.Update(It.IsAny<TRANSPORT>()))
                .Throws<Exception>();

            // Act / Assert
            Assert.That(this.transportsLogic.UpdateItem(id, column, newValue).StartsWith($"ERROR:"), Is.True);
        }

        /// <summary>
        /// UpdateItem() updates DRIVER_ID column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 1, "20")]
        public void UpdateItem_Updates_DRIVER_ID_Column(int id, int column, string newValue)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.transportsRepoMock.Object.GetAll().Where(a => a.ID_TRANSPORT == id).Single());

            // Act
            this.transportsLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.transportsRepoMock.Object.GetItemById(id).DRIVER_ID, Is.EqualTo(20));
        }

        /// <summary>
        /// UpdateItem() updates PASSENGER_ID column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 2, "20")]
        public void UpdateItem_Updates_PASSENGER_ID_Column(int id, int column, string newValue)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.transportsRepoMock.Object.GetAll().Where(a => a.ID_TRANSPORT == id).Single());

            // Act
            this.transportsLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.transportsRepoMock.Object.GetItemById(id).PASSENGER_ID, Is.EqualTo(20));
        }

        /// <summary>
        /// UpdateItem() updates DEPARTURE_TIME column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 3, "2007-07-07 07:00")]
        public void UpdateItem_Updates_DEPARTURE_TIME_Column(int id, int column, string newValue)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.transportsRepoMock.Object.GetAll().Where(a => a.ID_TRANSPORT == id).Single());

            // Act
            this.transportsLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.transportsRepoMock.Object.GetItemById(id).DEPARTURE_TIME.ToString().Contains("2007"), Is.True);
        }

        /// <summary>
        /// UpdateItem() updates DEPARTURE_ADDR_ID column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 4, "20")]
        public void UpdateItem_Updates_DEPARTURE_ADDR_ID_Column(int id, int column, string newValue)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.transportsRepoMock.Object.GetAll().Where(a => a.ID_TRANSPORT == id).Single());

            // Act
            this.transportsLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.transportsRepoMock.Object.GetItemById(id).DEPARTURE_ADDR_ID, Is.EqualTo(20));
        }

        /// <summary>
        /// UpdateItem() updates ARRIVE_TIME column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 5, "2007-07-07 07:07")]
        public void UpdateItem_Updates_ARRIVE_TIME_Column(int id, int column, string newValue)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.transportsRepoMock.Object.GetAll().Where(a => a.ID_TRANSPORT == id).Single());

            // Act
            this.transportsLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.transportsRepoMock.Object.GetItemById(id).ARRIVE_TIME.ToString().Contains("2007"), Is.True);
        }

        /// <summary>
        /// UpdateItem() updates DESTINATIN_ADDR_ID column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 6, "20")]
        public void UpdateItem_Updates_DESTINATION_ADDR_ID_Column(int id, int column, string newValue)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.transportsRepoMock.Object.GetAll().Where(a => a.ID_TRANSPORT == id).Single());

            // Act
            this.transportsLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.transportsRepoMock.Object.GetItemById(id).DESTINATION_ADDR_ID, Is.EqualTo(20));
        }

        /// <summary>
        /// UpdateItem() updates VEHICLE_ID column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 7, "20")]
        public void UpdateItem_Updates_VEHICLE_ID_Column(int id, int column, string newValue)
        {
            // Arrange
            this.transportsRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.transportsRepoMock.Object.GetAll().Where(a => a.ID_TRANSPORT == id).Single());

            // Act
            this.transportsLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.transportsRepoMock.Object.GetItemById(id).VEHICLE_ID, Is.EqualTo(20));
        }

        /// <summary>
        /// ListItems() if exception is thrown returns correct string
        /// </summary>
        [Test]
        public void ListItems_If_Exception_Thrown_Returns_Correct_String()
        {
            // Arrange
            this.transportsRepoMock
                .Setup(x => x.GetAll())
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.transportsLogic.ListItems().StartsWith("ERROR:"), Is.True);
        }

        /// <summary>
        /// ListItems() return string contains DB data
        /// </summary>
        [Test]
        public void ListItems_Return_String_Contains_Db_Data()
        {
            // Arrange

            // Act, Assert
            Assert.That(this.transportsLogic.ListItems().Contains("DRIVER_ID"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("PASSENGER_ID"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("DEPARTURE_TIME"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("DEPARTURE_ADDR_ID"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("ARRIVE_TIME"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("DESTINATION_ADDR_ID"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("VEHICLE_ID"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("2003.03.03 - 03:00"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("2003.03.03 - 03:03"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("2002.02.02 - 02:00"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("2002.02.02 - 02:02"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("2001.01.01 - 01:00"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("2001.01.01 - 01:01"), Is.True);
            Assert.That(this.transportsLogic.ListItems().Contains("4"), Is.True);
        }
    }
}
