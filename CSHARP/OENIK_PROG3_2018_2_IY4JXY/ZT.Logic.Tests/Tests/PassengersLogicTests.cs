﻿// <copyright file="PassengersLogicTests.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Tests.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Moq;
    using NUnit.Framework;
    using ZT.Data;
    using ZT.Logic.Logics;
    using ZT.Repository;

    /// <summary>
    /// Passengers table CRUD logic nunit tests
    /// </summary>
    [TestFixture]
    public class PassengersLogicTests
    {
        private Mock<IRepository<PASSENGER>> passengersRepoMock;
        private PassengersLogic passengersLogic;

        /// <summary>
        /// Gets three testCaseData for drivers logic methods
        /// </summary>
        public static IEnumerable<TestCaseData> ThreePassengersTestCaseData
        {
            get
            {
                List<TestCaseData> testCases = new List<TestCaseData>()
                {
                    new TestCaseData((object)new string[] { "vezeték4", "kereszt4", "4" }),
                    new TestCaseData((object)new string[] { "vezeték5", "kereszt5", "5" }),
                    new TestCaseData((object)new string[] { "vezeték6", "kereszt6", "6" }),
                };
                return testCases;
            }
        }

        /// <summary>
        /// Setup mock for passengers logic testing
        /// </summary>
        [SetUp]
        public void SetupPassengersRepo()
        {
            this.passengersRepoMock = new Mock<IRepository<PASSENGER>>();
            List<PASSENGER> passengersList = new List<PASSENGER>()
            {
                new PASSENGER() { ID_PASSENGER = 1, LAST_NAME = "vezet1", FIRST_NAME = "kereszt1", ADDRESS_ID = 1 },
                new PASSENGER() { ID_PASSENGER = 2, LAST_NAME = "vezet2", FIRST_NAME = "kereszt2", ADDRESS_ID = 2 },
                new PASSENGER() { ID_PASSENGER = 3, LAST_NAME = "vezet3", FIRST_NAME = "kereszt3", ADDRESS_ID = 3 }
            };

            this.passengersRepoMock
                .Setup(repo => repo.GetAll())
                .Returns(passengersList.AsQueryable());

            this.passengersRepoMock
                .Setup(repo => repo.Insert(It.IsAny<PASSENGER>()))
                .Callback<PASSENGER>(d => passengersList.Add(d));

            this.passengersRepoMock
                .Setup(repo => repo.Remove(It.IsAny<PASSENGER>()))
                .Callback<PASSENGER>(d => passengersList.Remove(d));

            this.passengersLogic = new PassengersLogic(this.passengersRepoMock.Object);
        }

        /// <summary>
        /// AddNewItem() method adds only one item
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "vezeték7", "kereszt7", "7" })]
        public void AddNewItem_Adds_Only_One_Item_To_Repository(string[] newData)
        {
            // Arrange

            // Act
            this.passengersLogic.AddNewItem(newData);

            // Assert
            Assert.That(this.passengersRepoMock.Object.GetAll().Count(), Is.EqualTo(4));
        }

        /// <summary>
        /// AddNewItem() successfull insert returns correct string
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "vezeték7", "kereszt7", "7" })]
        public void AddNewItem_Successfull_Insert_Returns_Correct_String_(string[] newData)
        {
            // Arrange

            // Act, Assert
            Assert.That(this.passengersLogic.AddNewItem(newData), Is.EqualTo("NEW ITEM ADDED!"));
        }

        /// <summary>
        /// AddNewItem() method adds correct item to repository
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCase((object)new[] { "vezeték7", "kereszt7", "7" })]
        public void AddNewItem_If_Exception_Thrown_Returns_Correct_String(string[] newData)
        {
            // Arrange
            this.passengersRepoMock
                .Setup(x => x.Insert(It.IsAny<PASSENGER>()))
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.passengersLogic.AddNewItem(newData).StartsWith("ERROR: "), Is.True);
        }

        /// <summary>
        /// AddNewItem() method adds correct item to repository
        /// </summary>
        /// <param name="newData">properties of new item</param>
        [TestCaseSource("ThreePassengersTestCaseData")]
        public void AddNewItem_Adds_Correct_Item_To_DriversRepo(string[] newData)
        {
            // Arrange

            // Act
            this.passengersLogic.AddNewItem(newData);

            // Assert
            Assert.That(this.passengersRepoMock.Object.GetAll().Last().LAST_NAME, Is.EqualTo(newData[0]));
            Assert.That(this.passengersRepoMock.Object.GetAll().Last().FIRST_NAME, Is.EqualTo(newData[1]));
            Assert.That(this.passengersRepoMock.Object.GetAll().Last().ADDRESS_ID, Is.EqualTo(int.Parse(newData[2])));
        }

        /// <summary>
        /// RemoveItem() deletes only one item from repo
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(2)]
        public void RemoveItem_Removes_Only_One_Item(int id)
        {
            // Arrange
            this.passengersRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.passengersRepoMock.Object.GetAll().Where(a => a.ID_PASSENGER == id).Single());

            // Act
            this.passengersLogic.RemoveItem(id);

            // Assert
            Assert.That(this.passengersRepoMock.Object.GetAll().Count(), Is.EqualTo(2));
        }

        /// <summary>
        /// RemoveItem() deletes item with given id
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void RemoveItem_Removes_Correct_Item(int id)
        {
            // Arrange
            this.passengersRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.passengersRepoMock.Object.GetAll().Where(a => a.ID_PASSENGER == id).Single());

            // Act
            this.passengersLogic.RemoveItem(id);

            // Assert
            Assert.That(this.passengersRepoMock.Object.GetAll().Any(x => x.ID_PASSENGER == id), Is.False);
        }

        /// <summary>
        /// RemoveItem() returns correct string if delete is successfull
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void RemoveItem_Successfull_Remove_Returns_Correct_String(int id)
        {
            // Arrange
            this.passengersRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.passengersRepoMock.Object.GetAll().Where(a => a.ID_PASSENGER == id).Single());

            // Act, Assert
            Assert.That(this.passengersLogic.RemoveItem(id), Is.EqualTo($"ROW WITH ID={id} IS DELETED!"));
        }

        /// <summary>
        /// RemoveItem() returns error message if id not found in db
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(-9999)]
        [TestCase(0)]
        [TestCase(9999)]
        public void RemoveItem_If_ID_Not_In_Db_Returns_Correct_String(int id)
        {
            // Arrange
            this.passengersRepoMock
                .Setup(repo => repo.Remove(It.IsAny<PASSENGER>()))
                .Throws<ArgumentNullException>();

            // Act / Assert
            Assert.That(this.passengersLogic.RemoveItem(id).StartsWith($"ERROR: Table contains no row with ID={id}"), Is.True);
        }

        /// <summary>
        /// RemoveItem() returns error message if exception is thrown
        /// </summary>
        /// <param name="id">Id of item to be deleted</param>
        [TestCase(2)]
        public void RemoveItem_If_Exception_Thrown_Returns_Correct_String(int id)
        {
            // Arrange
            this.passengersRepoMock
                .Setup(x => x.Remove(It.IsAny<PASSENGER>()))
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.passengersLogic.RemoveItem(id).StartsWith("ERROR: "), Is.True);
        }

        /// <summary>
        /// UpdateItem() succefull update returns correct string
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 1, "proba1")]
        [TestCase(1, 2, "proba2")]
        [TestCase(1, 3, "10")]
        public void UpdateItem_Succesfull_Update_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.passengersRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.passengersRepoMock.Object.GetAll().Where(a => a.ID_PASSENGER == id).Single());

            string columnName = " ";
            switch (column)
            {
                case 1:
                    columnName = "LAST_NAME";
                    break;
                case 2:
                    columnName = "FIRST_NAME";
                    break;
                case 3:
                    columnName = "ADDRESS_ID";
                    break;
                default:
                    break;
            }

            // Act / Assert
            Assert.That(this.passengersLogic.UpdateItem(id, column, newValue).StartsWith($"ROW WITH ID={id} MODIFIED! {columnName}: NEW VALUE='{newValue}'"), Is.True);
        }

        /// <summary>
        /// UpdateItem() returns error message if id not found in db
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(4, 1, "proba1")]
        public void UpdateItem_If_ID_Not_In_Db_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.passengersRepoMock
                .Setup(x => x.Update(It.IsAny<PASSENGER>()))
                .Throws<TargetException>();

            // Act / Assert
            Assert.That(this.passengersLogic.UpdateItem(id, column, newValue).Contains($"ERROR: Table contains no row with ID={id}"), Is.True);
        }

        /// <summary>
        /// UpdateItem() returns error message if exception is thrown
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(2, 1, "proba1")]
        public void UpdateItem_If_Exception_Thrown_Returns_Correct_String(int id, int column, string newValue)
        {
            // Arrange
            this.passengersRepoMock
                .Setup(x => x.Update(It.IsAny<PASSENGER>()))
                .Throws<Exception>();

            // Act / Assert
            Assert.That(this.passengersLogic.UpdateItem(id, column, newValue).StartsWith($"ERROR:"), Is.True);
        }

        /// <summary>
        /// UpdateItem() updates LAST_NAME column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 1, "proba1")]
        public void UpdateItem_Updates_LAST_NAME_Column(int id, int column, string newValue)
        {
            // Arrange
            this.passengersRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.passengersRepoMock.Object.GetAll().Where(a => a.ID_PASSENGER == id).Single());

            // Act
            this.passengersLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.passengersRepoMock.Object.GetItemById(id).LAST_NAME, Is.EqualTo("proba1"));
        }

        /// <summary>
        /// UpdateItem() updates FIRST column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 2, "proba2")]
        public void UpdateItem_Updates_FIRST_NAME_Column(int id, int column, string newValue)
        {
            // Arrange
            this.passengersRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.passengersRepoMock.Object.GetAll().Where(a => a.ID_PASSENGER == id).Single());

            // Act
            this.passengersLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.passengersRepoMock.Object.GetItemById(id).FIRST_NAME, Is.EqualTo("proba2"));
        }

        /// <summary>
        /// UpdateItem() updates ADDRESS_ID column
        /// </summary>
        /// <param name="id">id of item to modify</param>
        /// <param name="column">column of item to modify</param>
        /// <param name="newValue">new value for column</param>
        [TestCase(1, 3, "9")]
        public void UpdateItem_Updates_ADDRESS_ID_Column(int id, int column, string newValue)
        {
            // Arrange
            this.passengersRepoMock
                .Setup(repo => repo.GetItemById(id))
                .Returns(this.passengersRepoMock.Object.GetAll().Where(a => a.ID_PASSENGER == id).Single());

            // Act
            this.passengersLogic.UpdateItem(id, column, newValue);

            // Assert
            Assert.That(this.passengersRepoMock.Object.GetItemById(id).ADDRESS_ID, Is.EqualTo(9));
        }

        /// <summary>
        /// ListItems() if exception is thrown returns correct string
        /// </summary>
        [Test]
        public void ListItems_If_Exception_Thrown_Returns_Correct_String()
        {
            // Arrange
            this.passengersRepoMock
                .Setup(x => x.GetAll())
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.passengersLogic.ListItems().StartsWith("ERROR:"), Is.True);
        }

        /// <summary>
        /// ListItems() return string contains DB data
        /// </summary>
        [Test]
        public void ListItems_Return_String_Contains_Db_Data()
        {
            // Arrange

            // Act, Assert
            Assert.That(this.passengersLogic.ListItems().Contains("ID_PASSENGER"), Is.True);
            Assert.That(this.passengersLogic.ListItems().Contains("LAST_NAME"),    Is.True);
            Assert.That(this.passengersLogic.ListItems().Contains("FIRST_NAME"),   Is.True);
            Assert.That(this.passengersLogic.ListItems().Contains("ADDRESS_ID"),   Is.True);
            Assert.That(this.passengersLogic.ListItems().Contains("vezet1"),       Is.True);
            Assert.That(this.passengersLogic.ListItems().Contains("kereszt2"),     Is.True);
            Assert.That(this.passengersLogic.ListItems().Contains("3"),            Is.True);
            Assert.That(this.passengersLogic.ListItems().Contains("2"),            Is.True);
        }
    }
}
