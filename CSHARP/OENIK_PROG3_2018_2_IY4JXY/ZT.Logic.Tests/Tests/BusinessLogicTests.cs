﻿// <copyright file="BusinessLogicTests.cs" company="Zuber Transport">
// Copyright (c) Zuber Transport. All rights reserved.
// </copyright>

namespace ZT.Logic.Tests.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using ZT.Data;
    using ZT.Logic.Logics;
    using ZT.Repository;

    /// <summary>
    /// non CRUD business logic unit tests
    /// </summary>
    [TestFixture]
    public class BusinessLogicTests
    {
        private Mock<IRepository<ADDRESS>> addressRepoMock;
        private Mock<IRepository<DRIVER>> driversRepoMock;
        private Mock<IRepository<PASSENGER>> passengersRepoMock;
        private Mock<IRepository<TRANSPORT>> transportsRepoMock;
        private Mock<IRepository<VEHICLE>> vehiclesRepoMock;
        private BusinessLogic businessLogic;

        /// <summary>
        /// Setup mock repositorys for business logic
        /// </summary>
        [SetUp]
        public void SetupBusinessLogicRepos()
        {
            this.addressRepoMock = new Mock<IRepository<ADDRESS>>();
            this.driversRepoMock = new Mock<IRepository<DRIVER>>();
            this.passengersRepoMock = new Mock<IRepository<PASSENGER>>();
            this.transportsRepoMock = new Mock<IRepository<TRANSPORT>>();
            this.vehiclesRepoMock = new Mock<IRepository<VEHICLE>>();

            List<ADDRESS> addressList = new List<ADDRESS>()
            {
                new ADDRESS() { ID_ADDRESS = 1, ZIP_CODE = 1000, CITY = "város1", STREET_NAME = "utca1", STREET_TYPE = "utcatípus1", STREET_NUMBER = "házszám1" },
                new ADDRESS() { ID_ADDRESS = 2, ZIP_CODE = 2000, CITY = "város2", STREET_NAME = "utca2", STREET_TYPE = "utcatípus2", STREET_NUMBER = "házszám2" },
                new ADDRESS() { ID_ADDRESS = 3, ZIP_CODE = 3000, CITY = "város3", STREET_NAME = "utca3", STREET_TYPE = "utcatípus3", STREET_NUMBER = "házszám3" }
            };

            List<DRIVER> driversList = new List<DRIVER>()
            {
                new DRIVER() { ID_DRIVER = 1, LAST_NAME = "vezet1", FIRST_NAME = "kereszt1", STARTDATE_OF_WORK = new DateTime(2000, 07, 07), EXPERIENCE = 3, POINTS = 3 },
                new DRIVER() { ID_DRIVER = 2, LAST_NAME = "vezet2", FIRST_NAME = "kereszt2", STARTDATE_OF_WORK = new DateTime(2000, 08, 08), EXPERIENCE = 4, POINTS = 2 },
                new DRIVER() { ID_DRIVER = 3, LAST_NAME = "vezet3", FIRST_NAME = "kereszt3", STARTDATE_OF_WORK = new DateTime(2000, 09, 09), EXPERIENCE = 5, POINTS = 1 }
            };

            List<PASSENGER> passengersList = new List<PASSENGER>()
            {
                new PASSENGER() { ID_PASSENGER = 1, LAST_NAME = "vezet1", FIRST_NAME = "kereszt1", ADDRESS_ID = 1 },
                new PASSENGER() { ID_PASSENGER = 2, LAST_NAME = "vezet2", FIRST_NAME = "kereszt2", ADDRESS_ID = 2 },
                new PASSENGER() { ID_PASSENGER = 3, LAST_NAME = "vezet3", FIRST_NAME = "kereszt3", ADDRESS_ID = 3 }
            };

            List<TRANSPORT> transportsList = new List<TRANSPORT>()
            {
                new TRANSPORT() { ID_TRANSPORT = 1, DRIVER_ID = 1, PASSENGER_ID = 1, DEPARTURE_TIME = new DateTime(2001, 01, 01, 01, 00, 00), DEPARTURE_ADDR_ID = 1, ARRIVE_TIME = new DateTime(2001, 01, 01, 01, 01, 00), DESTINATION_ADDR_ID = 2, VEHICLE_ID = 1 },
                new TRANSPORT() { ID_TRANSPORT = 2, DRIVER_ID = 2, PASSENGER_ID = 2, DEPARTURE_TIME = new DateTime(2002, 02, 02, 02, 00, 00), DEPARTURE_ADDR_ID = 2, ARRIVE_TIME = new DateTime(2002, 02, 02, 02, 02, 00), DESTINATION_ADDR_ID = 3, VEHICLE_ID = 2 },
                new TRANSPORT() { ID_TRANSPORT = 3, DRIVER_ID = 2, PASSENGER_ID = 2, DEPARTURE_TIME = new DateTime(2003, 03, 03, 03, 00, 00), DEPARTURE_ADDR_ID = 3, ARRIVE_TIME = new DateTime(2003, 03, 03, 03, 03, 00), DESTINATION_ADDR_ID = 2, VEHICLE_ID = 3 },
                new TRANSPORT() { ID_TRANSPORT = 4, DRIVER_ID = 3, PASSENGER_ID = 3, DEPARTURE_TIME = new DateTime(2003, 03, 03, 03, 00, 00), DEPARTURE_ADDR_ID = 3, ARRIVE_TIME = new DateTime(2003, 03, 03, 03, 03, 00), DESTINATION_ADDR_ID = 2, VEHICLE_ID = 3 },
                new TRANSPORT() { ID_TRANSPORT = 5, DRIVER_ID = 3, PASSENGER_ID = 3, DEPARTURE_TIME = new DateTime(2003, 03, 03, 03, 00, 00), DEPARTURE_ADDR_ID = 3, ARRIVE_TIME = new DateTime(2003, 03, 03, 03, 03, 00), DESTINATION_ADDR_ID = 2, VEHICLE_ID = 3 },
                new TRANSPORT() { ID_TRANSPORT = 6, DRIVER_ID = 3, PASSENGER_ID = 3, DEPARTURE_TIME = new DateTime(2003, 03, 03, 03, 00, 00), DEPARTURE_ADDR_ID = 3, ARRIVE_TIME = new DateTime(2003, 03, 03, 03, 03, 00), DESTINATION_ADDR_ID = 2, VEHICLE_ID = 3 },
            };

            List<VEHICLE> vehiclesList = new List<VEHICLE>()
            {
                new VEHICLE() { ID_VEHICLE = 1, BRAND = "márka1", MODELL = "modell1", COLOR = "szín1", AGE_GROUP = 1980, SPEEDOMETER_STATE = 100000 },
                new VEHICLE() { ID_VEHICLE = 2, BRAND = "márka2", MODELL = "modell2", COLOR = "szín2", AGE_GROUP = 1990, SPEEDOMETER_STATE = 200000 },
                new VEHICLE() { ID_VEHICLE = 3, BRAND = "márka3", MODELL = "modell3", COLOR = "szín3", AGE_GROUP = 2000, SPEEDOMETER_STATE = 300000 }
            };

            this.addressRepoMock
                .Setup(repo => repo.GetAll())
                .Returns(addressList.AsQueryable());

            this.driversRepoMock
                .Setup(repo => repo.GetAll())
                .Returns(driversList.AsQueryable());

            this.passengersRepoMock
                .Setup(repo => repo.GetAll())
                .Returns(passengersList.AsQueryable());

            this.transportsRepoMock
                .Setup(repo => repo.GetAll())
                .Returns(transportsList.AsQueryable());

            this.vehiclesRepoMock
                .Setup(repo => repo.GetAll())
                .Returns(vehiclesList.AsQueryable());

            this.businessLogic = new BusinessLogic(
                this.addressRepoMock.Object,
                this.driversRepoMock.Object,
                this.passengersRepoMock.Object,
                this.transportsRepoMock.Object,
                this.vehiclesRepoMock.Object);
        }

        /// <summary>
        /// CreateTransportView() returns string contains DB data
        /// </summary>
        [Test]
        public void CreateTransportView_Returns_String_Contains_Db_Data()
        {
            // Arrange

            // Act, Assert
            // Assert.That(this.businessLogic.CreateTransportsView().Contains("2003-03-03 03:03:00"), Is.True);
            Assert.That(this.businessLogic.CreateTransportsView().Contains("város1"), Is.True);
            Assert.That(this.businessLogic.CreateTransportsView().Contains("utca2"), Is.True);
            Assert.That(this.businessLogic.CreateTransportsView().Contains("utcatípus3"), Is.True);
            Assert.That(this.businessLogic.CreateTransportsView().Contains("házszám1"), Is.True);
            Assert.That(this.businessLogic.CreateTransportsView().Contains("3000"), Is.True);
            Assert.That(this.businessLogic.CreateTransportsView().Contains("kereszt2"), Is.True);
            Assert.That(this.businessLogic.CreateTransportsView().Contains("vezet1"), Is.True);
            Assert.That(this.businessLogic.CreateTransportsView().Contains("kereszt3"), Is.True);
            Assert.That(this.businessLogic.CreateTransportsView().Contains("márka1"), Is.True);
            Assert.That(this.businessLogic.CreateTransportsView().Contains("modell2"), Is.True);
            Assert.That(this.businessLogic.CreateTransportsView().Contains("szín3"), Is.True);
        }

        /// <summary>
        /// CreateTransportView() if exception is thrown returns correct string
        /// </summary>
        [Test]
        public void CreateTransportView_If_Exception_Thrown_Returns_Correct_String()
        {
            // Arrange
            this.addressRepoMock
                .Setup(x => x.GetAll())
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.businessLogic.CreateTransportsView().StartsWith("ERROR:"), Is.True);
        }

        /// <summary>
        /// GetTop3Customers() returns correct string
        /// </summary>
        [Test]
        public void GetTop3Customers_Returns_Correct_String()
        {
            // Arrange

            // Act, Assert
            Assert.That(this.businessLogic.GetTop3Customers().Contains($"vezet1 kereszt1 --> 1x"), Is.True);
            Assert.That(this.businessLogic.GetTop3Customers().Contains($"vezet2 kereszt2 --> 2x"), Is.True);
            Assert.That(this.businessLogic.GetTop3Customers().Contains($"vezet3 kereszt3 --> 3x"), Is.True);
        }

        /// <summary>
        /// GetTop3Customers() if exception is thrown returns correct string
        /// </summary>
        [Test]
        public void GetTop3Customers_If_Exception_Thrown_Returns_Correct_String()
        {
            // Arrange
            this.passengersRepoMock
                .Setup(x => x.GetAll())
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.businessLogic.GetTop3Customers().StartsWith("ERROR:"), Is.True);
        }

        /// <summary>
        /// GetTop3DriversbyDrives() returns correct string
        /// </summary>
        [Test]
        public void GetTop3DriversByDrives_Returns_Correct_String()
        {
            // Arrange

            // Act, Assert
            Assert.That(this.businessLogic.GetTop3DriversByDrives().Contains($"vezet1 kereszt1 --> 1x"), Is.True);
            Assert.That(this.businessLogic.GetTop3DriversByDrives().Contains($"vezet2 kereszt2 --> 2x"), Is.True);
            Assert.That(this.businessLogic.GetTop3DriversByDrives().Contains($"vezet3 kereszt3 --> 3x"), Is.True);
        }

        /// <summary>
        /// GetTop3DriversByDrives() if exception is thrown returns correct string
        /// </summary>
        [Test]
        public void GetTop3DriversByDrives_If_Exception_Thrown_Returns_Correct_String()
        {
            // Arrange
            this.driversRepoMock
                .Setup(x => x.GetAll())
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.businessLogic.GetTop3DriversByDrives().StartsWith("ERROR:"), Is.True);
        }

        /// <summary>
        /// GetTop3DriversbyPoints() returns correct string
        /// </summary>
        [Test]
        public void GetTop3DriversByPoints_Returns_Correct_String()
        {
            // Arrange

            // Act, Assert
            Assert.That(this.businessLogic.GetTop3DriversByPoint().Contains($"vezet1 kereszt1 --> POINTS=3"), Is.True);
            Assert.That(this.businessLogic.GetTop3DriversByPoint().Contains($"vezet2 kereszt2 --> POINTS=2"), Is.True);
            Assert.That(this.businessLogic.GetTop3DriversByPoint().Contains($"vezet3 kereszt3 --> POINTS=1"), Is.True);
        }

        /// <summary>
        /// GetTop3DriversByPoint() if exception is thrown returns correct string
        /// </summary>
        [Test]
        public void GetTop3DriversByPoint_If_Exception_Thrown_Returns_Correct_String()
        {
            // Arrange
            this.driversRepoMock
                .Setup(x => x.GetAll())
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.businessLogic.GetTop3DriversByPoint().StartsWith("ERROR:"), Is.True);
        }

        /// <summary>
        /// GetAverageCarAge() returns correct string
        /// </summary>
        [Test]
        public void GetAverageCarAge_Returns_Correct_String()
        {
            // Arrange

            // Act, Assert
            Assert.That(this.businessLogic.GetAverageCarAge().Contains("AVG=1990"), Is.True);
        }

        /// <summary>
        /// GetAverageCarAge() if exception is thrown returns correct string
        /// </summary>
        [Test]
        public void GetAverageCarAge_If_Exception_Thrown_Returns_Correct_String()
        {
            // Arrange
            this.vehiclesRepoMock
                .Setup(x => x.GetAll())
                .Throws<Exception>();

            // Act, Assert
            Assert.That(this.businessLogic.GetAverageCarAge().StartsWith("ERROR:"), Is.True);
        }
    }
}
